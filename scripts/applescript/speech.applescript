on run
    global gContinueProcessing
    set gContinueProcessing to true
    repeat
        if gContinueProcessing is true then
            try
                tell application "SpeechRecognitionServer"
                    set theReply to ""
                    set theReply to listen continuously for {"finder", "sherlock", "done"} with identifier "hello" with prompt "Say one finder, sherlock or done."
                    if theReply is "finder" then
                        tell application "Finder" to activate
                    else if theReply is "sherlock" then
                        tell application "Sherlock" to activate
                    else if theReply is "done" then
                        say "Later [[emph -]]dude." displaying "Later, dude."
                        exit repeat
                    end if
                end tell
            on error
                exit repeat
            end try
        end if
    end repeat
    --  when the repeat is exited stop listening
    if gContinueProcessing is true then
        try
            tell application "SpeechRecognitionServer"
                stop listening for identifier "hello"
                set gContinueProcessing to false
            end tell
        end try
        tell me to quit
    end if
end run