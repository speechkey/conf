#!/bin/sh
#Check befor commit if any js files staged
#compress new version of each file and stage it for current commit

JS=`git diff --name-only --diff-filter=ACMRT --cached | grep \\.js$ | grep -v \\.min.js$`

if [ `echo $JS | wc -l` -ge 1 ]
then
	for f in $JS
	do
		yuicompressor $f --type js -o ${f%%js}min.js
		git add ${f%%js}min.js
	done
fi
