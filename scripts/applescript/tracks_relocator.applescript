tell application "iTunes"
	set storagePath to "/Volumes/NAS/Audio/Music/"
	
	set selectedTracks to selection
	if selectedTracks is {} then return
	
	if (get class of (item 1 of selectedTracks)) is not file track then return
	
	repeat with trk in selectedTracks
		try
			with timeout of 300 seconds
				set track_artist to (artist of trk as string) # if empty
				if track_artist is equal to "" then
					set track_artist to "Unknown Artist"
				end if
				
				set track_album to (album of trk as string) # if empty
				if track_album is equal to "" then
					set track_album to "Unknown Album"
				end if
				
				set track_location to (location of trk as string)
				set file_name to name of (info for alias track_location)
				set location_sfx to (track_artist & "/" & track_album)
				
				do shell script "mkdir -p " & my quofo(storagePath & location_sfx) & " && cp -p " & my quofo(track_location) & " " & my quofo(storagePath & location_sfx & "/")
				set chk to do shell script "if [ -f " & my quofo(storagePath & location_sfx & "/" & file_name) & " ]; then echo 1; fi"
				
				if chk is "1" then
					my relocate(trk, (POSIX file (storagePath & location_sfx & "/" & my file_name) as text))
					do shell script "rm -rf " & my quofo(track_location)
					my logger((POSIX path of track_location) & " -> " & storagePath & location_sfx & "/" & my file_name)
				end if
			end timeout
		on error m
			my logger(m)
		end try
	end repeat
end tell

on quofo(t)
	return (quoted form of POSIX path of (t as text))
end quofo

on relocate(t, nf)
	tell application "iTunes" to set location of t to (nf as alias)
	tell application "iTunes" to set comment of t to "Remote on NAS"
end relocate

to logger(t)
	log t
	try
		do shell script "logger -t Relocator " & quoted form of (t as text)
	end try
end logger
