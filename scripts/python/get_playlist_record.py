# -*- coding: utf-8 -*-
'''
Created on Dec 29, 2011

@author: iRoot
'''
import os.path
import urllib2
import re
#import gzip
#import getopt
#import sys
from StringIO import StringIO
from BeautifulSoup import BeautifulSoup
from multiprocessing import Process, Queue


class SmartRedirectHandler(urllib2.HTTPRedirectHandler):
    def http_error_301(self, req, fp, code, msg, headers):
        result = urllib2.HTTPRedirectHandler.http_error_301(
            self, req, fp, code, msg, headers)
        result.status = code
        return result

    def http_error_302(self, req, fp, code, msg, headers):
        result = urllib2.HTTPRedirectHandler.http_error_302(
            self, req, fp, code, msg, headers)
        result.status = code
        return result


class DefaultErrorHandler(urllib2.HTTPDefaultErrorHandler):
    def http_error_default(self, req, fp, code, msg, headers):
        result = urllib2.HTTPError(
            req.get_full_url(), code, msg, headers, fp)
        result.status = code
        return result


def testFunc(url):
    return 0


class HTTPFetcher():
    '''
    asdf
    '''
    ETAG = None
    USER_AGENT = "Python"
    GZIP = 1
    LAST_MODIFIED = None
    URL = None

    def __open_page(self, url=URL, agent=USER_AGENT, etag=ETAG, gzip=GZIP, lastmodified=LAST_MODIFIED):
        '''
        Test
        @param test string asdflsadf
        '''
        request = urllib2.Request(url)
        request.add_header('User−Agent', agent)

        if etag:
            request.add_header('If−None−Match', etag)
        if lastmodified:
            request.add_header('If−Modified−Since', lastmodified)
        if gzip:
            request.add_header('Accept−encoding', 'gzip')

        opener = urllib2.build_opener(SmartRedirectHandler(), DefaultErrorHandler())
        try:
            return opener.open(request)
        except IOError:
            print "Could not open document: %s" % url
            return 0
        '''except HTTPError, e:
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
        except URLError, e:
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason'''

    def fetch(self, url=URL, agent=USER_AGENT, etag=ETAG, gzip=GZIP, lastmodified=LAST_MODIFIED, parseFunc=None):
        result = {}
        f = self.__open_page(url, agent, etag, gzip, lastmodified)
        result['data'] = f.read()

        if hasattr(f, 'headers'):
            # save ETag, if the server sent one
            self.ETAG = f.headers.get('ETag')
            # save Last−Modified header, if the server sent one
            self.LAST_MODIFIED = f.headers.get('Last−Modified')
        if f.headers.get('content−encoding', '') == 'gzip':
            # data came back gzip−compressed, decompress it
            result['data'] = gzip.GzipFile(fileobj=StringIO(result['data'])).read()
        if hasattr(f, 'url'):
            result['url'] = f.url
            result['status'] = 200
        if hasattr(f, 'status'):
            result['status'] = f.status
        if parseFunc:
            result['parsed'] = parseFunc(result['data'])
        f.close()
        return result

'''
Created on Dec 26, 2010
get-record-pls [--playlist <names>] [[--track-count] | [--track <track number>] | --all]
@author: iRoot
'''


class RecordPlaylist():
    '''
    This class extracts mps songs URLs from Radio Record Playlists
    http://www.radiorecord.ru/radio/playlists
    '''

    def __init__(self, baseURL):
        '''
        Initialize object with base URL of the playlist
        '''
        self.__baseURL = baseURL

    def __getPageContent(self, pageNumber=None):
        '''
        Return page content

        :pageNumber int Number of processed page
        :return string Page XML
        '''
        if pageNumber:
            return HTTPFetcher().fetch(self.__baseURL + "&PAGEN_1=" + str(pageNumber))["data"]
        else:
            return HTTPFetcher().fetch(self.__baseURL)["data"]

    def getPageCount(self):
        '''
        Return number of pages in playlist

        :return int Return number of pages in the playlist
        '''
        xml = HTTPFetcher().fetch(self.__baseURL)["data"]
        soup = BeautifulSoup(xml)
        return int(soup.find("div", {"class": "navigation"}). \
                        find("li", {"class": "active"}). \
                        contents.pop(0))

    def getPlaylist(self, page_number=None):
        '''
        Return links to the all tracks in the playlist

        :return list(string) List with all tracks URLs in the playlist
        '''
        if page_number:
            xml = HTTPFetcher().fetch(self.__baseURL + "&PAGEN_1=" + str(page_number))["data"]
        else:
            xml = HTTPFetcher().fetch(self.__baseURL)["data"]
        soup = BeautifulSoup(xml)
        print xml
        return ["http://www.radiorecord.ru%s" % item['href'] for item in soup.findAll("a", {"class": "inline-exclude"})]

    def __processXML(self, xml):
        '''
        doc
        '''
        songs = []
        soup = BeautifulSoup(xml)
        for item in self.__getURL(\
                    soup.find("ul", {"class": "playlists"}).\
                             findAll("script")):
                songs.append(item)
        return songs

    def getLastTrackURL(self):
        '''
        Return link to the last track

        :return string URL of the last track in the playlist
        '''
        return self.getPlaylist(self)[0]

    def getArtist(self, track):
        artistNamePattern = re.compile(r'http://www.radiorecord.ru/audio/playlists/(.*)/.*\.mp3')
        return artistNamePattern.search(track).group(1)


def worker(input, artistPattern, fileNamePattern, path):
    #Get next Playlist URL from queue and process if not a 'STOP' — end of the queue
    for host in iter(input.get, 'STOP'):
        rp = RecordPlaylist(host)
        #get newest track in playlist
        track = rp.getLastTrackURL()
        #get artist name, to create a folder for playlists if not exists
        artist = rp.getArtist(track)
        #get file name
        file_name = fileNamePattern.search(track).group(1) + ".m3u"
        #create folder for artists playlists if not exists
        abs_dir_path = os.path.expanduser(path + "/" + artist)
        if not os.path.exists(abs_dir_path):
            os.makedirs(abs_dir_path)
        #create playlist if not exists
        abs_file_path = abs_dir_path + "/" + file_name
        if not os.path.exists(abs_file_path):
            FILE = open(abs_file_path, "w")
            FILE.writelines(track)
            FILE.close()
            print abs_file_path

def worker(input, artistPattern, fileNamePattern, path):
    #Get next Playlist URL from queue and process if not a 'STOP' — end of the queue
    for host in iter(input.get, 'STOP'):
        rp = RecordPlaylist(host)
        #get newest track in playlist
        track = rp.getLastTrackURL()
        #get artist name, to create a folder for playlists if not exists
        artist = rp.getArtist(track)
        #get file name
        file_name = fileNamePattern.search(track).group(1) + ".m3u"
        #create folder for artists playlists if not exists
        abs_dir_path = os.path.expanduser(path + "/" + artist)
        if not os.path.exists(abs_dir_path):
            os.makedirs(abs_dir_path)
        #create playlist if not exists
        abs_file_path = abs_dir_path + "/" + file_name
        if not os.path.exists(abs_file_path):
            FILE = open(abs_file_path, "w")
            FILE.writelines(track)
            FILE.close()
            print abs_file_path


def main():
    """Runs everything"""

    #Links to the playlists
    hosts = ['http://www.radiorecord.ru/radio/playlists/208/', 'http://www.radiorecord.ru/radio/playlists/200/', 'http://www.radiorecord.ru/radio/playlists/201/', 'http://www.radiorecord.ru/radio/playlists/205/', 'http://www.radiorecord.ru/radio/playlists/1916/']
    #Regexp to extract artists name from the link
    artistPattern = re.compile(r'http://www.radiorecord.ru/audio/playlists/(.*)/.*')
    #Regexp to extract file name from the link
    fileNamePattern = re.compile(r'http://www.radiorecord.ru/audio/playlists/.*/(.*)\.mp3')
    #Work with each playlist in separate process
    NUMBER_OF_PROCESSES = len(hosts)
    #Path to the folder with playlists
    path = "~/Downloads/Record Playlists"
    #Create folder if not exists
    abs_dir_path = os.path.expanduser(path)
    if not os.path.exists(abs_dir_path):
        os.makedirs(abs_dir_path)

    # Create queues
    task_queue = Queue()

    #submit tasks
    for host in hosts:
        task_queue.put(host)

    #Start worker processes
    for i in range(NUMBER_OF_PROCESSES):
        Process(target=worker, args=(task_queue, artistPattern, fileNamePattern, path)).start()

    #Adding queue end for child process
    for i in range(NUMBER_OF_PROCESSES):
        task_queue.put('STOP')


if __name__ == '__main__':
    main()


'''
MAX_WORKERS = 3
PLAYLISTS = {"LadyWachs": {"url": "http://www.radiorecord.ru/radio/playlists/208/?SECTION_ID=208", "showtime": 0, "method": None, "args": {}},
             "ArminVanBuren": {"url": "http://www.radiorecord.ru/", "showtime": 0, "method": None, "args": {}}}


def usage():
    print "usage"

if __name__ == '__main__':
      if len(sys.argv) > 1:
        print sys.argv
        try:
            opts, args = getopt.getopt(sys.argv[1:], "ho:v", ["help", "playlist=", "track-count", " track=", "all"])
        except getopt.GetoptError, err:
            print str(err)
            usage()
            sys.exit(2)

        for key, val in opts:
            if key == "--all":
                queue = Queue.Queue(0)
                for i in range(MAX_WORKERS):
                    Worker(queue).start()
                for artist, pls in PLAYLISTS:
                    pls["method"] = "getAllURL"
                    queue.put({artist: pls})
                for i in range(MAX_WORKERS):
                    queue.put(None)
                print "all"  # Get all tracks from all playlist
                break
            elif key == "--playlist":
                if len(opts) > 1:
                    for key, val in opts:
                        if key == "--track-count":
                            print "track count"
                            break
                        elif key == "--track":
                            if val == "all":
                                print "get all track"
                                break
                            else:
                                break
                        elif key == "--playlist":
                                pass
                        else:
                            usage()
                            break
                else:
                    usage()
                    break
            elif key in ("--track-count", "--track"):
                pass
            else:
                pass
    else:
        usage()

    start = time.time()
    q = Queue.Queue(0)
    urls = ["http://www.radiorecord.ru/radio/playlists/208/?SECTION_ID=208" + "&PAGEN_1=" + str(i) for i in range(3)]
    for url in urls:
        try:
            thread = threading.Thread(target=HTTPFetcher(q).fetch, args=[url])
            thread.start()
            print thread.getName()
            thread.join()
        except:
            raise
    print time.time() - start
    start = time.time()
    q = Queue.Queue(0)
    urls = ["http://www.radiorecord.ru/radio/playlists/208/?SECTION_ID=208" + "&PAGEN_1=" + str(i) for i in range(3)]
    for url in urls:
        try:
            HTTPFetcher().fetch(url)
        except:
            raise
    print time.time() - start
    item = RecordPlaylist("http://www.radiorecord.ru/radio/playlists/208/?SECTION_ID=208")
    print item.getAllURL()'''
