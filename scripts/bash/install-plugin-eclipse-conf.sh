/Applications/eclipse/eclipse \
-configuration ~/Dev/SDK/eclipse/Google\ Apps\ Engine\ for\ Java/configuration \
-application org.eclipse.equinox.p2.director -noSplash \
-repository http://download.eclipse.org/releases/juno,http://download.eclipse.org/eclipse/updates/4.2,\
https://dl.google.com/eclipse/plugin/4.2 \
-installIUs \
com.google.appengine.eclipse.sdkbundle.feature.feature.group, \
com.google.gdt.eclipse.suite.e42.feature.feature.group \
-vmargs -Declipse.p2.mirrors=true -Djava.net.preferIPv4Stack=true
