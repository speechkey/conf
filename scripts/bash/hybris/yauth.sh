#!/usr/bin/env bash
#
# The script authenticates on Hybris Administration Console and prints
# cookies and csrf can be used by other scripts for future communication.
#
# Usage:
#   '''
#      . ./hybris_auth.sh
#      echo $YAUTH_COOKIE
#      echo $YAUTH_CSRF
#   '''
#

#Configuration
HAC_USERNAME="admin"
HAC_PASSWORD="nimda"
HAC_DOMAIN="localhost"
HAC_PORT="9001"
HAC_CONTEXT_PATH=""
HAC_URL=$HAC_DOMAIN:$HAC_PORT$HAC_CONTEXT_PATH

# Authorize client
PARAMS=$(curl --silent http://$HAC_URL/login.jsp | sed -n -e '/meta.*_csrf/{p;n;}' -e '/<form/{p;n;}' | sed 's/.*name=\"_csrf\" content=\"\(.*\)\".*/\1/' | sed 's/.*action=\"\([^"]*\)\".*/\1/' | tr '\n' ' ')
AUTH_CSRF=$(echo $PARAMS | cut -d ' ' -f 1)
ACTION_URL=$(echo $PARAMS | cut -d ' ' -f 2)
COOKIE=$(echo $ACTION_URL | cut -d ';' -f 2)

AUTH_COOKIE=$(curl -s -i --data "_csrf=$AUTH_CSRF&j_username=$HAC_USERNAME&j_password=$HAC_PASSWORD" --cookie "$COOKIE" http://$HAC_URL$ACTION_URL | sed '/Set-Cookie: JSESSIONID.*/!d' | sed 's/Set-Cookie: \(JSESSIONID=[^ ]*\);.*/\1/')

CSRF=$(curl -i -s --cookie "$AUTH_COOKIE" http://$HAC_URL/platform/update | sed -n -e '/meta.*_csrf/{p;n;}' | sed 's/.*name=\"_csrf\" content=\"\(.*\)\".*/\1/')

# export tokens to the parent shell
export YAUTH_COOKIE=$AUTH_COOKIE
export YAUTH_CSRF=$CSRF
