# -*- coding: utf-8 -*-
'''
Created on Dec 26, 2010
./check_inet.py
@author: iRoot

If run from command line returns LRZ internet status.
OK:
    Status: ok
BLOCKED:
    Status: blocked
    Day: dd
    Month: mm
    Year: yy
    Hour: hh
    Minute: mm

    @TODO: Save blocked ports in mongoDB
    @TODO: Cron job if blocked, save ports and processes using ports in mongoDB
    @TODO: On internet becomes unavailable show grow notification, on internet becomes available show grow notification and play sound
'''

import urllib2
import sys
import re
sys.path.append("/Users/iRoot/Dev/libs/python2")
from BeautifulSoup import BeautifulSoup


class CheckInet:

    url = 'http://secomat3.srv.lrz.de/whyme.cgi'
    user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'

    def __init__(self):
        self.__getPageContent()

    def __getPageContent(self):
        data = None
        header = {'User-agent': self.user_agent}

        Request = urllib2.Request
        req = Request(self.url, data, header)
        handle = urllib2.urlopen(req)

        self.pageXML = handle.read()

    def getStatus(self):
        soup = BeautifulSoup(self.pageXML)
        statusStr = soup.find("h3").font.contents.pop(0)
        if statusStr == "Keine Sperrung / Not blocked":
            return {"status": "ok"}
        else:
            m = re.match(r".*since (\d{2}).(\d{2}).(\d{2})&nbsp;(\d{2}):(\d{2})", statusStr)
            return {"status": "blocked", "day": m.group(1), "month": m.group(2), "year": m.group(3), "hour": m.group(4), "minute": m.group(5)}

    def getConnections(self):
        soup = BeautifulSoup(self.pageXML)
        connections = [[td.contents[0].contents[0], td.contents[1].contents[0], td.contents[2].contents[0]] for td in soup.find("h3").nextSibling.findAll("tr") if not td.b]
        return connections

if __name__ == '__main__':
    check = CheckInet()
    status = check.getStatus()
    if (status["status"] == "ok"):
        print "Status: ok"
    else:
        print "Status: blocked"
        print "Day: " + status['day']
        print "Month: " + status['month']
        print "Year: " + status['year']
        print "Hour: " + status['hour']
        print "Minute: " + status['minute']
