#!/bin/sh
#Check befor commit if css files staged
#compress new version of each file and stage it for current commit

CSS=`git diff --name-only --diff-filter=ACMRT --cached | grep \\.css$ | grep -v \\.min.css$`
if [ `echo $CSS | wc -l` -ge 1 ]
then
	for f in $CSS
	do
		yuicompressor $f --type css -o ${f%%css}min.css
		git add ${f%%css}min.css
	done
fi
