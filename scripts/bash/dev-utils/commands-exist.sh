###################################################################################
#
# Checks if commands are available on the target system.
# The function is suppose to be running from another bash script for checking 
# its external dependencies.
#
# Example: commands_available file which pngcruch
#
# Author: Artem Grebenkin <artem.grebenkin@gmail.com>
# Date: 19.09.2013
#
###################################################################################

commands_available () {
	#Missing commands
	MISSING=""
	#Iterate over all commands and check if they are available
	for CMD in $@; do
		hash $CMD 2>/dev/null || >&2 MISSING="$MISSING, $CMD"
	done
	# If some commands are missing prtint their names and exit
	if [ ! -z "$MISSING" ]; then
		# Trim waste coma by first command item
		MISSING=${MISSING#", "}
		# Print warning and exit
		echo "Error: $MISSING binaries are missing and should be installed before using the script." | \
		$SCRIPTS_PATH/bash/dev-utils/colorize-shell.sh red 'Error:' white $MISSING
		exit 1
	fi
}