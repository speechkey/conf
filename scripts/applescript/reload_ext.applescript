set ext_to_reload to {"###EXT_NAME###"}
set k to 0

tell application "Safari" to activate
tell application "System Events" to tell process "Safari"
	click menu item "Show Extension Builder" of menu 1 of menu bar item "Develop" of menu bar 1
	delay 1
	tell UI element 1 of scroll area 1 of window "Extension Builder"
		set tc to (count (groups whose its images is not {}))
		repeat with i from 1 to tc
			if exists (static text 1 of group i) then
				set t_name to name of static text 1 of group i
				if t_name is in ext_to_reload then
					click button "Reload" of UI element ("ReloadUninstall" & t_name)
					set k to k + 1
					if k = (count ext_to_reload) then exit repeat
					delay 1
				end if
			end if
			keystroke (character id 31)
			delay 0.2
		end repeat
		delay 0.8
		keystroke "w" using command down
		delay 1
		tell window 1
			keystroke "r" using command down
		end tell
	end tell
end tell
