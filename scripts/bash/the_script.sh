LOG_FILE=/tmp/logfile.log
# Close STDOUT file descriptor
exec 1<&-
# Close STDERR FD
exec 2<&-

# Open STDOUT as $LOG_FILE file for read and write.
exec 1<>$LOG_FILE

# Redirect STDERR to STDOUT
exec 2>&1
echo "The script"
source ~/.local_env_vars
# Cache var configuration in the temp file
TMP_VARS_FILE=/tmp/local_env_vars;
# Ensure there is no already such file
rm -rf $TMP_VARS_FILE
# Export all local environment variables
sed 's/export \(.*\)=\(.*\)/launchctl setenv \1 \2/' ~/.local_env_vars > $TMP_VARS_FILE;
# Export all environment variables in sourced files
grep '^source' ~/.local_env_vars | sed 's/source \(.*\)/\1/' | while read line; do eval cat $line; done | sed 's/export \(.*\)=\(.*\)/launchctl setenv \1 \2/' >> $TMP_VARS_FILE;
# Apply vars
source $TMP_VARS_FILE
