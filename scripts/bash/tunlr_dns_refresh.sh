#! /usr/bin/env bash
###
# me.sh
#
# Updates Pandora IP in /etc/hosts file. Real Pandora IPs are provided by http://tunlr.net
# Script check if IP has changed and updates it in /etc/hosts.
#
# @permisions sudo
#
# Created by Artem Grebenkin.
# Copyright (c) 2013 Artem Grebenkin <artem.grebenkin@gmail.com>. All rights reserved.
#
###
# indicates if changes has been done
STATUS=0
# DNS
DNS_SERVER_IP=23.21.43.50
# real pandora IPs
WWW_PANDORA_COM=`dig www.pandora.com @$DNS_SERVER_IP +short`
TUNER_PANDORA_COM=`dig tuner.pandora.com @$DNS_SERVER_IP +short`
T1_1_P_CDN_COM=`dig t1-1.p-cdn.com @$DNS_SERVER_IP +short`
CONT_1_P_CDN_COM=`dig cont-1.p-cdn.com @$DNS_SERVER_IP +short`
MEDIASERVER_SV5_RT_1_PANDORA_COM=`dig mediaserver-sv5-rt-1.pandora.com @$DNS_SERVER_IP +short`
# hosts pandora IPs
HOSTS_WWW_PANDORA_COM=`grep www.pandora.com /etc/hosts | sed -e 's/\([0-9\.]*\) www.pandora.com/\1/'`
HOSTS_TUNER_PANDORA_COM=`grep tuner.pandora.com /etc/hosts | sed -e 's/\([0-9\.]*\) tuner.pandora.com/\1/'`
HOSTS_T1_1_P_CDN_COM=`grep t1-1.p-cdn.com /etc/hosts | sed -e 's/\([0-9\.]*\) t1-1.p-cdn.com/\1/'`
HOSTS_CONT_1_P_CDN_COM=`grep cont-1.p-cdn.com /etc/hosts | sed -e 's/\([0-9\.]*\) cont-1.p-cdn.com/\1/'`
HOSTS_MEDIASERVER_SV5_RT_1_PANDORA_COM=`grep mediaserver-sv5-rt-1.pandora.com /etc/hosts | sed -e 's/\([0-9\.]*\) mediaserver-sv5-rt-1.pandora.com/\1/'`

if [ $WWW_PANDORA_COM != $HOSTS_WWW_PANDORA_COM ]
then
	sed -e "s/\([0-9\.]*\) www.pandora.com/$WWW_PANDORA_COM www.pandora.com/" -i.bak /etc/hosts
	STATUS=1
fi

if [ $TUNER_PANDORA_COM != $HOSTS_TUNER_PANDORA_COM ]
then
	sed -e "s/\([0-9\.]*\) tuner.pandora.com/$TUNER_PANDORA_COM tuner.pandora.com/" -i.bak /etc/hosts
	STATUS=1
fi
if [ $T1_1_P_CDN_COM != $HOSTS_T1_1_P_CDN_COM ]
then
	sed -e "s/\([0-9\.]*\) t1-1.p-cdn.com/$T1_1_P_CDN_COM t1-1.p-cdn.com/" -i.bak /etc/hosts
	STATUS=1
fi
if [ $CONT_1_P_CDN_COM != $HOSTS_CONT_1_P_CDN_COM ]
then
	sed -e "s/\([0-9\.]*\) cont-1.p-cdn.com/$CONT_1_P_CDN_COM cont-1.p-cdn.com/" -i.bak /etc/hosts
	STATUS=1
fi
if [ $MEDIASERVER_SV5_RT_1_PANDORA_COM != $HOSTS_MEDIASERVER_SV5_RT_1_PANDORA_COM ]
then
	sed -e "s/\([0-9\.]*\) mediaserver-sv5-rt-1.pandora.com/$MEDIASERVER_SV5_RT_1_PANDORA_COM mediaserver-sv5-rt-1.pandora.com/" -i.bak /etc/hosts
	STATUS=1
fi
# Remove sed backup files sed has processed. See http://blog.remibergsma.com/2012/09/18/sed-inline-editing-different-on-mac-osx/
if [ $STATUS -eq 1 ]
then
#	rm -rf /etc/hosts.bak
	echo "Pandora IP has been changed."
fi
