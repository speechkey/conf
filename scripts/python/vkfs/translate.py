#!/usr/bin/env python
# -*- coding: UTF-8 -*-

tr = {
'groups':'Groups',
'friends':'Friends',
'audio':'Audios',
'video':'Videos',
'photo':'Photo albums',
'avatar':'Avatar',
'info':'Info',
'subscriptions':'Subscriptions',
'followers':'Followers',
'docs':'Documents'
}

def _(*arg):
	pass

_('Groups')
_('Friends')
_('Audios')
_('Videos')
_('Photo albums')
_('Avatar')
_('Info')
_('Subscriptions')
_('Followers')
