#sync_links.sh <source_dir> <target_dir>
#Create soft links to all files from source directory in target directory, if not already exists
#
source=$1
target=$2
prefix=$3
#Set bash delimiter to \n 
IFS=$'\n'
#Check if source directory path has / on the end
lastchar=$(echo $source | awk '{print substr($0,length($0),length($0))}')
if [ "/" != "$lastchar" ]
then
	source=$source/
fi

lastchar=$(echo $target | awk '{print substr($0,length($0),length($0))}')
if [ "/" != "$lastchar" ]
then
	target=$target/
fi

for file in `find "$source" -type f`
do
	linkname=`basename $file`
	if [ ! -h "$target$prefix$linkname" ]
	then
		ln -s $file $target$prefix$linkname
	fi
done
#Unset IFS to its default
unset $IFS
