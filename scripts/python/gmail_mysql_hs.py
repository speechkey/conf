#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Search gremoz@gmail.com mailbox for emails from steffen.mueller@hotelsnapper.de
and subject sql, extract sql and print to stdout

@author: speechkey
'''
import re
import gmail_imap
import keychain

if __name__ == '__main__':
    kchn = keychain.Keychain()
    crd = kchn.get_internet_password('cli', 'gremoz', 'gremoz@Gmail\ 4\ gmail_mysql_hs\.py')

    gmail = gmail_imap.gmail_imap('gremoz@gmail.com', crd['password'])
    gmail.messages.search("Job/HotelSnapper", "(FROM steffen.mueller@hotelsnapper.de) (ALL SUBJECT sql)")
    gmail.messages.process()

    for msg in gmail.messages[0:10]:
        message = gmail.messages.getMessage(msg.uid)
        #If message contains html, cut it out
        if message.type == 'text/html':
            #cut html out
            import html2text
            h = html2text.HTML2Text()
            h.ignore_links = True
            h.ignore_images = True
            h.google_doc = False
            h.ignore_emphasis = True
            query = h.handle(message.Body.decode(message.charset))
            #remove message signature
            cleanQuery = re.compile(r'((.*\n)*?)[\s|\n]*?--[\s|\n]*?Steffen Mueller')
            matchResult = cleanQuery.match(query)
            if(matchResult):
                query = matchResult.group(1)
            else:
                #handle unable to find signature
                print message.Body
            #split bunch of queries in a list of single queries and add ";" on the end
            #if not already has
            import sqlparse
            for x in sqlparse.split(query):
                if len(x) > 5:
                    if x[-1:] == ";":
                        print x.rstrip()
                    else:
                        print x.rstrip() + ";"
        else:
            #remove signature
            cleanQuery = re.compile(r'((.*\n)*?)[\s|\n]*?--[\s|\n]*?Steffen Mueller')
            matchResult = cleanQuery.match(message.Body)
            if(matchResult):
                print matchResult.group(1)
            else:
                #handle unable to find signature
                print message.Body

    gmail.logout()
