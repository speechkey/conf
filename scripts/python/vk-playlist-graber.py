# -*- coding: utf-8 -*-
import urllib2
import os
import lxml
import lxml.html
import re
import webbrowser


def checkWget():
    '''Проверяется наличие в системе wget'''

    print "Проверяю наличие wget. Сейчас будет скачана какая-то пое**нь..."
    testfilename = "x_8091546b.jpg"
    cmd = "wget http://cs5705.vkontakte.ru/u403273/139647416/x_8091546b.jpg"
    os.popen(cmd)
    chk = os.path.exists(testfilename)
    if chk == True:
        print "Да, wget у тебя, как видишь, стоит. Все пучком, приступаем!"
        os.remove(testfilename)
    else:
        print "Ну вот, не стоит. Ставь wget, а потом уже лезь качать что-то!"
        exit()

    return

print "Привет, юзер! Ты запустил скрипт grabVK. Сейчас мы с тобой скачаем музло с твоей страницы."
print "Данный скрипт скачивает все подряд музло, делая, скажем так, бэкап музыки с твоей страницы."
print "Ну а дальне ты сможешь слушать свои же записи где хочешь и каким хочешь плеером"
print "Ну что ж, начнем"
print "Напомню, что для правильно работы скрипта, в ней должен быть установлен wget!"
print "Есть ли у тебя в системе wget?"
print "Если ты уверен, что у тебя стоит - смело жмакай y, если нет - n, тогда я проверю сам =)"

answer = raw_input("Стоит ли у тебя wget?: ")
if answer == "y":
    print "Ну раз стоит, так стоит. Не шуми потом, если ничего не выйдет!"
else:
    checkWget()

print "Мы же хотим по-быстрому скачать музыку, так? Поэтому было решено не клепать велосипедов и открыть нужные ссылки в системной браузере"
print "Но есть один нюанс - после того, как откроется браузер и ты пройдешь авторизацию - не закрывай его сломя голову!"
print "После прохождения авторизации ты видишь непонятную строчку на экране, так? Молодец!"
print "В строчке, как ты можешь видеть, есть три параметра, это access_token, expires_in и user_id, разделенные знакои & "
print "Скопируй по очереди эти параметры и введи в соответствующие поля скрипта, нажав ентер"

answer = raw_input("Готов?: ")
if answer == "y":
    print "Вот и хорошо, продолэжаем!"
else:
    print "Вот же какой трусливый! Ну и ладно!"
    exit()

webbrowser.open_new_tab("http://api.vkontakte.ru/oauth/authorize?client_id=2647463&scope=audio&redirect_uri=http://api.vk.com/blank.html&display=page&response_type=token")

access_token = raw_input("access_token: ")
print "Малаца, теперь копипасть expires_in."
expires_in = raw_input("expires_in: ")
print "Ну ваще мегакулхацкер прям! Остался последний параметр."
user_id = raw_input("user_id: ")
print "Миссия завершена! Таперича начинаем дергать музло. Точнее пока что коннектиться и дратьс писок. Вперде!"

print "Ну, была не была, коннектимся!"

url = "https://api.vkontakte.ru/method/audio.get.xml?uid=" + user_id + "&access_token=" + access_token
page = urllib2.urlopen(url)
html = page.read()

print "Список музла получен, парсим..."

artistMas = []
titleMas = []
urlMas = []
number = 0

print "Парсим на предмет исполнителей..."

doc = lxml.html.document_fromstring(html)
for artist in doc.cssselect('artist'):
    artistMas.append(artist.text)
    number = number + 1

print "OK"
print "Парсим на предмет названий..."

for title in doc.cssselect('title'):
    titleMas.append(title.text)

print "OK"
print "Парсим на предмет ссылок..."

for urlm in doc.cssselect('url'):
    urlMas.append(urlm.text)

print "OK"

print ""

print "Ну вот, теперь приступаем к даунлоадингу! Помни, юзер, что скачается весь плейлист!"
print "Т.е. если у тебя дофига записей и медленный интернет - забей на это дело и жди гуевую"
print "морду, где можно будет качать выборочно"

path = "download"
if os.path.exists(path):
    "Папка уже есть, начинаем докачку "
else:
    os.makedirs(path)

print "Нам нужно скачать кучу файлов. Вычисляем количество..."
print number

answer = raw_input("Готов?: ")
if answer == "y":
    print "Пошла закачка, пошла родимая!"
else:
    print "Вот же какой трусливый! Ну и ладно!"
    exit()


for i in xrange(0, number - 1):
    print "Загружается:"
    print i
    print " "

    filename_new = path + "/" + artistMas[i] + " - " + titleMas[i] + ".mp3"
    if os.path.exists(filename_new):
        print "Этот файл уже загружен, переходим к следующему"
    else:
        downCmd = "wget -P" + path + " " + urlMas[i]
        os.popen(downCmd)

        p = re.compile(r"[0-9a-zA-Z]+\.mp3$")
        filename = p.findall(urlMas[i])

        try:
            os.rename(path + "/" + filename[0], path + "/" +
            artistMas[i] + " - " + titleMas[i] + ".mp3")
        except:
            print "Невозможно переименовать, оставляю изначальное имя файла!"

    print " "

print "Задание завершено! Удачи!"
