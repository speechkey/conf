#!/usr/bin/env bash
##################################################################################
#
# Checks java files on pmd issues with given ruleset. Is able to print
# execution log to stdout and define priority level to fail. Exits with 1 if
# pmd issues with priority over level was found or with 1 if not.
#
# Usage:
#       inspect-pmd.sh --silent --level 3 --ruleset ~/pmd-ruleset.xml ~/TestClass.java ~/TestClass2.java
#
#            --ruleset: Path to the pmd ruleset
#            --silent: Optional. Don't log pmd output
#            --level: Optional. Pmd notification level to fail, default `3`
#
# Author: Artem Grebenkin <speechkey@gmail.com>
# Date: 09.10.2014
#
##################################################################################

# For using scripts in dev-utils
PATH=$PATH:$SK_BIN/bash/dev-utils
# Check dependencies
check-cmd-exists.sh pmd || exit 1

/usr/local/bin/pmd pmd -d /Users/agrebenkin/Dev/xplace/repo/hybris/bin/custom/slstorefront/web/src/de/simply/local/slstorefront/web/product/CMSProductOffersComponentController.java -f xml -R ~/Dev/xplace/repo/scripts/coding_conventions/rulesets/pmd.xml -version 1.7 -language java
