Quick setup of equal shell user expirience on the new workstation
=================================================================

1. Clone repositry: `git@bitbucket(speechkey):speechkey/conf.git` and remember its local directory.
2. Invoke `./install.sh` to generate configuration files.
3. Override default configuration at the end of generated files.

*TODO*
1. Place `install.sh` in public to be able to download anywhere.
2. Add cloning directory in the `install.sh` to avoid remembering local repository directory.
3. Test on new Mac (by removing config)
4. Test on linux VM
5. One time environment on foreign hosts -> invoke once without download and lost on exit: `wget http://me | source`
6. Test shell_collors working in bash and zsh
7. File to export vars to GUI check, conditions before exports
8. Introduce shell_env global
9. Init zsh plugins after checking binaries in `zshrc`
