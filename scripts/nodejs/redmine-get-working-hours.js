#!/usr/bin/env node
require('date-utils');

var api_key = '57ebb26c8cc9bf373d41ef7d903c1b3bee321ac4';
var status_id = 5;
var assigned_to = 27;
var host = 'support.cyberday-gmbh.de';
var http = require('https');

var options = {
  host: host,
  port: 443,
  path: '/projects/hotelsnapper/issues.json?key=' + api_key + '&status_id=' + status_id + '&assigned_to_id=' + assigned_to + '&limit=1000',
  method: 'GET'
};

var currentDate = Date.today().getDate();

if(currentDate > 20){
	beginDate = new Date(Date.today().toFormat('YYYY-MM').toString() + '-20');
	endDate = new Date(Date.parse(beginDate)).addMonths(1);
} else {
	beginDate = new Date(Date.today().toFormat('YYYY').toString() + '-' + parseInt(Date.today().getMonth()) + '-20');
	endDate = new Date(Date.parse(beginDate)).addMonths(1);
}

var req = http.request(options, function(res) {
  res.setEncoding('utf8');
  var data = '';
  res.on('data', function (chunk) {
    data += chunk;
  });

  res.on('end', function(){
     data = JSON.parse(data);
     var total_count = data.total_count;
     var limit = data.limit;
     var offset = data.offset;
     var time = 0;
     for(var i in data.issues){
	if(data.issues[i].due_date){
		if((new Date(data.issues[i].due_date)).between(beginDate, endDate)){
       			time += parseFloat(data.issues[i].estimated_hours) * 60;
			
		}
	}	
     }
	console.log(time/60);
  })
});

// write data to request body
req.write('data\n');
req.write('data\n');
req.end();
