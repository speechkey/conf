tell application "iTunes"
    if selection is not {} then
        set sel to selection
        set these_titles to {}
        set these_files to {}
        with timeout of 30000 seconds
            repeat with this_track in sel
                if class of this_track is file track then
                    set end of these_titles to ((name of this_track) as string)
                    set end of these_files to (get location of this_track)
					log (name of this_track) as string
					log (get location of this_track) as string
                end if
            end repeat
        end timeout
        if these_files is not {} then
            --my email_files()
        else
            display dialog "Selected tracks cannot be processed." buttons {"Cancel"} default button 1 with icon 2 giving up after 15
        end if
    else
        display dialog "No tracks selected." buttons {"Cancel"} default button 1 with icon 2 giving up after 15
    end if
end tell

to email_files()
    set theRecipientName to text returned of (display dialog ("Who are you sending these to?") default answer "")
    tell application "Mail"
        repeat with i from 1 to the count of the these_files
            set thisFile to item i of these_files
            set thisTitle to item i of these_titles
            set newMessage to make new outgoing message with properties {subject:thisTitle, content:thisTitle & return & return}
            tell newMessage
                make new to recipient at end of to recipients with properties {address:theRecipientName}
                set visible to true
                tell content
                    make new attachment with properties {file name:thisFile} at after the last word of the second paragraph
                end tell
                activate
            end tell
        end repeat
    end tell
end email_files