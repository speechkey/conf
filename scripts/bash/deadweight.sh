#!/usr/bin/env bash
CSS=`grep "\\.css" $1 | sed 's/.*href="\(.*\)css.*".*/\1/'`
CSS_f=""
for f in $CSS
do
	CSS_f="$CSS_f -s $f"css
done
deadweight $CSS_f $1
