#!/bin/sh

########################################################
# Validate HTML of staged html files befor commit.
#
# Author: Artem Grebenkin <artem.grebenkin@gmail.com>
# Date: 09/07/2013
########################################################

# Get staged html files
HTML=`git diff --name-only --diff-filter=ACMRT --cached | grep \\.html$`
# Count staged html files
if [ `echo $HTML | wc -l` -ge 1 ]
then
# Validate staged html files and write output to the var
        for f in $HTML
        do
		OUTPUT_TMP="\nFile: "$f"\n\n"`git show :$f | $SCRIPTS_PATH/python/html-validator.py -h`
		if [ `echo $OUTPUT_TMP | grep 'Error\|Warning' | wc -l` != "0" ]
		then
			OUTPUT=$OUTPUT$OUTPUT_TMP"\n";
		fi
        done
# If an error or warning is found print an output to the console and abort commit
	if [ `echo $OUTPUT | grep "From line" | wc -l` != "0" ]
	then
		echo  "$OUTPUT" | $SCRIPTS_PATH/bash/dev-utils/colorize-shell.sh red 'Error:' yellow 'Warning:' blue 'File:'
		exit 1
	fi
fi
