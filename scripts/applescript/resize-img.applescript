on resizeImg(imgPaths)
	(* Get resolution to be resized to. *)
	set displayString to "Please input a number"
	(* Default resolution hier*)
	set defaultResolution to 500
	repeat
		set response to display dialog displayString default answer defaultResolution
		try
			set resolution to (text returned of response) as number
			exit repeat
		on error errstr
			set displayString to errstr & return & "Please try again."
			set defaultResolution to text returned of response
		end try
	end repeat
	(* Resize every given image to the resolution*)
	repeat with anImage in imgPaths
	tell application "Image Events"
		set the targetSize to resolution
		set currentImage to open anImage
		set imageType to currentImage's file type
		scale currentImage to size targetSize
		tell application "Finder" to set newImage to (container of anImage as string) & "scaled " & (name of anImage)
			save currentImage in newImage as imageType
		end tell
	end repeat
end resizeImg
