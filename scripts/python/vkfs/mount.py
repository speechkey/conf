#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import time, random
import urllib, urllib2

import fuse
import stat, os, errno, sys
import commands

import vkapi

CP="utf-8"
RTIME=300
TIMEOUT=5
#~ log = open('/tmp/vkfs.log','w')
		
class Entry():
	def __init__(self):
		self.attr = fuse.Stat()
		self.attr.st_uid = os.getuid()
		self.attr.st_gid = os.getgid()
		self.attr.st_ino = 0
		self.attr.st_dev = 0
		self.attr.st_size = 0
		self.attr.st_atime = 0
		self.attr.st_mtime = 0
		self.attr.st_ctime = 0

		self.src=""
		self.media=""
		self.id=0
		self.entries=[]

	def keys(self):
		return []


def array2text(a):
	t=''
	for info in a:
		t = ','.join([t,str(info)])
	t=t[1:]
	return t 

SEP='/'
BUFFERSIZE=65536
fuse.fuse_python_api = (0, 2)

class VKFS(fuse.Fuse):
	listdir = {}
	def adddir(self,path,id=None,media=None):
		print '*** adddir', path
		self.root[path] = Entry()
		self.root[path].attr.st_mode  = stat.S_IFDIR | 0555
		self.root[path].attr.st_nlink = 3
		self.root[path].attr.st_mtime = 0
		self.root[path].id = id
		self.root[path].media = media
		if path != SEP:
			paths = path.split(SEP)
			updir = SEP.join(paths[:-1])
			if updir == '' : updir = SEP
			if not paths[-1] in self.root[updir].entries:
				self.root[updir].entries.append(paths[-1])

	def addfile(self,path,src=''):
		# print >> log, '*** addfile',path,src
		self.root[path] = Entry()
		self.root[path].attr.st_mode = stat.S_IFREG | 0444
		self.root[path].attr.st_nlink = 1
		self.root[path].src=src
		if path != SEP:
			paths = path.split(SEP)
			updir = SEP.join(paths[:-1])
			if updir == '' : updir = SEP
			if not paths[-1] in self.root[updir].entries:
				self.root[updir].entries.append(paths[-1])
		self.fcount +=1
		
	def addusers(self,uids):

		infos=[{'first_name': 'Artem', 'nickname': 'speechkey', 'last_name': 'Grebenkina', 'uid': 104184112, 'photo_big': 'Me.jpg', }]
		paths=[]
		for info in infos:
			path = info['first_name']+' '+info['nickname']+' '+info['last_name']
			path = self.api.unescape(path)
			path = SEP + path.replace(SEP,"\\").encode(CP)
			self.adddir(path,info['uid'])
			self.root[path].media = 'user'
			for d in ['audio', 'photo', 'friends', 'video','subscriptions','followers','groups','docs']:
				subpath = path + SEP + d
				self.adddir(subpath, info['uid'])
				self.root[subpath].media = d
			self.addfile(path + SEP + 'Avatar' + '.' + info['photo_big'].split('.')[-1], info['photo_big'])
			#~ query['format'] = 'XML'
			#~ query['uids']   = info['uid']
			#~ query['fields'] = 'nickname,domain,sex,bdate,city,country,photo_big,contacts,education,online'
			#~ self.addfile(path+SEP+tr('info')+'.xml',self.api.getlink(query))
			paths.append(path)
		return paths
	def listdocs(self,path):

		docs = vkapi.docs()
		docs.api=self.api
		infos = docs.get(uid=self.root[path].id)
		try:
			infos=infos['response']
		except:
			infos=[]
		for doc in infos[1:]:
			filename = doc['title']
			filename = filename.replace(SEP,"\\").encode(CP)
			src = doc['url'].replace("\/","/")
			if filename.split('.')[-1] != doc['ext'] : filename = filename +'.'+doc['ext']
			self.addfile(path + SEP + filename, src)
			
		
	def listaudios(self,path):
		updir = SEP.join(path.split(SEP)[:-1])
		if updir == '' : updir = SEP
		firstletter=self.root[updir].media[0]
		
		query = {'method':'audio.get',firstletter+'id':self.root[path].id}
		infos = self.api.gettree(query)
		try:
			infos=infos['response']
		except:
			infos=[]
		for audio in infos:
			# print >> log, '*** audio', audio
			filename = self.api.unescape(audio['artist'] + ' - ' + audio['title'])
			filename = filename.replace(SEP,"\\").encode(CP)
			#id = audio['aid']
			src = audio['url'].replace("\/","/")
			self.addfile(path + SEP + filename+'.'+src.split('.')[-1], src)
			
	def listvideos(self,path):
		updir = SEP.join(path.split(SEP)[:-1])
		if updir == '' : updir = SEP
		firstletter=self.root[updir].media[0]
		query = {'method':'video.get','width': '320',firstletter+'id':self.root[path].id}
		infos = self.api.gettree(query)
		print '*** video.get', infos
		try:
			infos=infos['response'][1:]
		except:
			infos=[]
#~ {'description': u'\u0412\u0438\u0434\u0435\u043e \u2116 741<br>http://vkontakte.ru/club1265528<br>http://vkontakte.ru/board.php?act=topic&amp;tid=6928961<br>http://vkontakte.ru/photos.php?act=album&amp;id=52446152',
#~ 'vid': 86549753,
#~ 'title': u'\u0414\u0438\u0442\u0430 \u0444\u043e\u043d \u0422\u0438\u0437 / Dita Von Teese',
#~ 'image': 'http://436.gt3.vkadre.ru/assets/thumbnails/3058688677499984.160.vk.jpg',
#~ 'player': 'http://vkontakte.ru/video_ext.php?oid=7839870&id=86549753&hash=63a2798f466c1b23',
#~ 'date': 1232403477,
#~ 'link': 'video86549753',
#~ 'duration': 335,
#~ 'owner_id': 7839870}

		for info in infos:
			#~ print '*** video', info
			filename = self.api.unescape(info['title'])
			filename = filename.replace(SEP,"\\").encode(CP)
			src = self.api.parsevideo(info['player'].replace("\/","/"))
			if src == None: src = info['image']
			filename = path + SEP + filename+'.'+src.split('.')[-1]
			self.addfile(filename, src)
			self.utime(filename,(info['date'],info['date']))
			
	
	
	def listalbum(self,path):
		updir = SEP.join(path.split(SEP)[:-2])
		if updir == '' : updir = SEP
		firstletter=self.root[updir].media[0]
		
		query = {'method':'photos.get',firstletter+'id':self.root[updir].id, 'aid':self.root[path].id }
		#~ print query
		infos = self.api.gettree(query)
		try:
			infos=infos['response']
		except:
			infos=[]
		for info in infos:
			# print >> log, '*** foto', info
			filename = self.api.unescape(info['pid'])
			filename = filename.replace(SEP,"\\").encode(CP)
			#id = audio['aid']
			try:
				src = info['src_xxbig'].replace("\/","/")
			except:
				try:
					src = info['src_xbig'].replace("\/","/")
				except:
					src = info['src_big'].replace("\/","/")	
							
			self.addfile(path + SEP + filename + '.' + src.split('.')[-1], src)
			
	def listalbums(self,path):
		updir = SEP.join(path.split(SEP)[:-1])
		if updir == '' : updir = SEP
		firstletter=self.root[updir].media[0]
		
		query = {'method':'photos.getAlbums',firstletter+'id':self.root[path].id}
		infos = self.api.gettree(query)
		try:
			infos=infos['response']
		except:
			infos=[]
		for info in infos:
			# >> log,
			print '*** album', info
			dirname = self.api.unescape(info['title'])
			dirname = dirname.replace(SEP,"\\").encode(CP)
			self.adddir(path + SEP + dirname, info['aid'], 'album')
			self.root[path + SEP + dirname].attr.st_ctime = int(info['created'])
			self.root[path + SEP + dirname].attr.st_mtime = int(info['updated'])
			
			if (self.root[path].id == self.api.mid) or (int(info['owner_id']) < 0) or (int(info['owner_id']) == int(self.api.mid)):
				self.root[path + SEP + dirname].attr.st_mode  = stat.S_IFDIR | 0755
				
	def listfriends(self,path):
		#~ updir = SEP.join(path.split(SEP)[:-1])
		#~ if updir == '' : updir = SEP
		#~ firstletter=self.root[updir].media[0]
		
		query = {'method':'friends.get','uid':self.root[path].id,'timestamp':time.time(),'random':random.random()}
		infos = self.api.gettree(query)
		#~ print infos
		try:
			infos=infos['response']
		except:
			infos=[]
		for link in self.addusers(array2text(infos)):
			self.symlink(link,path+link)
			
	def listsubscriptions(self,path):
		subscriptions = vkapi.subscriptions()
		subscriptions.api = self.api
		infos=subscriptions.get(uid=self.root[path].id)['users']
		for link in self.addusers(array2text(infos)):
			self.symlink(link,path+link)

	def listfollowers(self,path):
		subscriptions = vkapi.subscriptions()
		subscriptions.api = self.api
		infos=subscriptions.getFollowers(uid=self.root[path].id)['users']
		for link in self.addusers(array2text(infos)):
			self.symlink(link,path+link)

	def addalbum(self,path):
		print '*** addalbum',
		photos=vkapi.photos()
		photos.api=self.api
		infos=photos.createAlbum(path.split(SEP)[-1])
		print infos
		
		
		
	def listgroups(self,path):
		print '*** listgroups', path
		query = {'method':'getGroupsFull','uid':self.root[path].id}
		infos = self.api.gettree(query)
		print '****',infos
		try:
			infos=infos['response']
		except:
			infos=[]
		#uids=''
		for info in infos:
			group = SEP + info['name'].replace(SEP,'\\').encode(CP)
			subpath = SEP + '.'+_('Groups') + group
			self.adddir(subpath, info['gid'],'group')
			self.adddir(subpath+SEP+_('Audios'),info['gid'],'audio')
			self.adddir(subpath+SEP+_('Photo'),info['gid'],'photo')
			#~ self.adddir(subpath+SEP+tr('video'),info['gid'],'video')
			self.addfile(subpath+SEP+_('Avatar')+'.'+info['photo'].split('.')[-1],info['photo'])
			#~ self.addfile(subpath+'.'+info['photo'].split('.')[-1],info['photo'])
			self.symlink(subpath,path + group)
			
	def symlink(self,target,name):
		print '*** symlink',target,name
		self.adddir(name,self.root[target].id,self.root[target].media)
		self.root[name].src=target
		self.root[name].attr.st_mode = stat.S_IFLNK | 0755
	
	def readlink(self, path):
		print '*** readlink:',path
		if self.root[path].media in ['user','group']:
			return '../..'+self.root[path].src
			#надо проверять уровень вложенности

				
									
	def __init__(self, *args, **kw):
		fuse.Fuse.__init__(self, *args, **kw)
		
		self.listdir['audio'] = self.listaudios
		self.listdir['photo'] = self.listalbums				
		self.listdir['album'] = self.listalbum
		self.listdir['friends'] = self.listfriends
		self.listdir['video'] = self.listvideos		
		self.listdir['groups'] = self.listgroups
		self.listdir['subscriptions'] = self.listsubscriptions		
		self.listdir['followers'] = self.listfollowers
		self.listdir['docs'] = self.listdocs
		
		self.api    = vkapi.vk()
		self.api.checkauth()
		self.fcount = 0
		self.root   = {}
		self.adddir(SEP, '104184112')
		path = self.addusers('104184112')[0]
		self.adddir(SEP + '.'+'Groups','104184112','allgroups')
		
	def statfs(self):
		# print >> log, '*** statfs'
		# Вернем информацию в объекте класса fuse.StatVfs
		#DefaultStatVfs()
		st = fuse.StatVfs()
		# Размер блока
		st.f_bsize   = 0
		st.f_frsize  = 1024
		st.f_bfree   = 0
		st.f_bavail  = 0
		# Количество файлов
		st.f_files   = self.fcount 
		# Количество блоков
		# Если f_blocks == 0, то 'df' не включит ФС в свой список - понадобится сделать 'df -a'
		st.f_blocks  = 1
		st.f_ffree   = 0
		st.f_favail  = 0
		st.f_namelen = 255
		return st
		
	def readdir(self, path, offset):
		# print >> log, '*** readdir:' + path
		# print >> log, offset
		if self.root[path].attr.st_atime < time.time() - RTIME:
			try:
				self.listdir[self.root[path].media](path)
			except KeyError:
				print '*** unknown dir type', self.root[path].media
		
		self.root[path].attr.st_atime = time.time()
		yield fuse.Direntry('.')
		yield fuse.Direntry('..')

		for key in self.root[path].entries:
			yield fuse.Direntry(key)

	def getattr(self, path):
		print '*** getattr:' + path
		if   path == SEP + '.'  : path = SEP
		elif path == SEP + '..' : path = SEP
		elif path.split(SEP)[-1] == '.' : path = path[:-2]
		elif path.split(SEP)[-1] == '..': path = path[:-3]
		
		if not path in self.root.keys():
			print '*** getattr: no such file'
			return -errno.ENOENT
		
		if (self.root[path].src[0:4] == 'file'):
			if os.path.isfile(self.root[path].src.split('file:/')[-1]):
				self.root[path].attr.st_size = os.path.getsize(self.root[path].src.split('file:/')[-1])

		elif (self.root[path].attr.st_size == 0) and (self.root[path].attr.st_atime < time.time() - RTIME):
			if (self.root[path].src[0:4] == 'http'):
				self.root[path].attr.st_atime = time.time()
				try:
					meta = urllib2.urlopen(self.root[path].src,timeout=TIMEOUT).info()
					self.root[path].attr.st_size = int(meta['Content-Length']) #размер файла
				except:
					self.root[path].attr.st_mode = stat.S_IFREG | 0000
					self.root[path].attr.st_size = 0
		try:
			return self.root[path].attr
		except:
			return -errno.ENOENT
				
	def open(self, path, flags):
		print '*** open:' , path, flags
		try:
			stat = self.root[path].attr
		except:
			return -errno.ENOENT

	def read(self, path, size, offset=0):
		print '*** read:' , path, size, offset, 'src:', self.root[path].src
		if self.root[path].src[:4] == 'http':
			url = self.root[path].src
			req = urllib2.Request(url)
			req.add_header('Range', 'bytes=%s-%s' % (offset,size+offset-1))
			return urllib2.urlopen(req,timeout=TIMEOUT).read(size)
			#~ data=urllib2.urlopen(req,timeout=TIMEOUT).read(size)
			#~ if data != None: return data
			#~ else: return ''
		elif self.root[path].src[:4] == 'file':
			f=open(self.root[path].src.split('file:/')[-1], "rb")
			f.seek(offset)
			return f.read(size)
			f.close()
		else:
			return ''

	def mknod(self, path, mode, dev):
		print '*** mknod', path , mode , dev
		updir = SEP.join(path.split(SEP)[:-1])
		if updir == '': updir=SEP
		if (mode & stat.S_IFREG > 0) and (self.root[updir].media in ['album','audio']):
			self.addfile(path,'file://tmp/' + ''.join(path.split(SEP)[-1].split('.')[:-1]) + str(int(time.time())) +'.'+ path.split('.')[-1])
			self.root[path].media = 'cache'
			self.root[path].attr.st_mode = stat.S_IFREG | 0666
			return 0
			
	def mkdir ( self, path, mode ):
		print '*** mkdir', path, oct(mode)
		updir = SEP.join(path.split(SEP)[:-1])
		if (self.root[updir].media in ['photo']):
			self.addalbum(path)
			self.listalbums(updir)
			return 0
		return -errno.ENOSYS
			
	def rename ( self, oldPath, newPath ):
		print '*** rename', oldPath, newPath
		updir = SEP.join(newPath.split(SEP)[:-1])
		if (self.root[oldPath].media in ['album']):
			print '*** editalbum',
			photos=vkapi.photos()
			photos.api=self.api
			aid=self.root[oldPath].id
			infos=photos.editAlbum(aid,newPath.split(SEP)[-1])
			self.listalbums(updir)
			return 0
		return -errno.ENOSYS
		
	def utime(self, path, times):
		print '*** utime', path, times
		now = int(time.time())
		self.root[path].attr.st_atime = 0
		self.root[path].attr.st_mtime = times[1]
		self.root[path].attr.st_ctime = times[0]
		return 0
		
	def write(self, path, buf, offset=0):
		print '*** write', path, len(buf), offset, buf[:4].encode('hex')
		f = open(self.root[path].src.split('file:/')[-1],'ab+')
		f.seek(offset)
		f.write(buf)
		f.flush()
		f.close()
		return len(buf)
		
	def release(self, path, flags):
		print '*** release', path ,flags
		updir = SEP.join(path.split(SEP)[:-1])
		ownerdir = SEP.join(path.split(SEP)[:-3])
		if ownerdir == '' : ownerdir = SEP
		if updir == '' : updir = SEP
		#print 		path[-3:].upper(),self.root[updir].media
		if (path[-3:].upper() in ['JPG','PNG','GIF']) and (self.root[updir].media == 'album') and (self.root[path].src[:4] == 'file') and (flags == 32769):
				gid = None
				if self.root[ownerdir].media == 'group' : gid = self.root[ownerdir].id
				uploader=vkapi.upload()
				uploader.api=self.api
				if uploader.photos(self.root[updir].id, self.root[path].src.split('file:/')[-1], gid) >= 0:
					commands.getoutput('rm ' + self.root[path].src.split('file:/')[-1])
					del self.root[path]
					self.listalbum(updir)
		elif (path[-3:].upper() in ['MP3']) and (self.root[updir].media == 'audio') and (self.root[path].src[:4] == 'file') and (flags == 32769):
				gid = None
				if self.root[ownerdir].media == 'group' : gid = self.root[ownerdir].id
				uploader=vkapi.upload()
				uploader.api=self.api
				if uploader.audio(self.root[path].src.split('file:/')[-1], gid) >= 0:
					commands.getoutput('rm ' + self.root[path].src.split('file:/')[-1])
					del self.root[path]
					self.listaudios(updir)

		return 0
			
def runFS():
	
	print sys.argv,__file__,sys.argv[-1][-5:0]
	argv = sys.argv
	sys.argv = []
	sys.argv.append('vkfs')
	for arg in argv:
		if arg[0] == '-': sys.argv.append(arg)
	if len(argv) < 2 or argv[-1][0] == '-': 
		mountpoint=os.path.expanduser('~')+'/vkfs'
	else:
		mountpoint=argv[-1]
		
	if not os.path.isdir(mountpoint):
		os.makedirs(mountpoint)
		
	sys.argv.append(mountpoint)

	usage = 'Vkontakte FS ' + fuse.Fuse.fusage +'\n if mountpoint is emty or error run auth first.'
	fs    = VKFS(version="%prog " + fuse.__version__,usage=usage,dash_s_do='setsingle')
	fs.parse(errex=1)
	#fs.flags = 0
	#fs.multithreaded = 1
	try:
		#fs.main()
		return fs
	except:
		print usage

if __name__ == '__main__':

    runFS()
