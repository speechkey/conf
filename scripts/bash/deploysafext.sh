ABSOLUTE_SCRIPT_PATH="$(dirname $(stat -f $0))"
FILE_PREF=$1;
EXT_NAME=$2;
sed -e 's/var resPath = ".*";/var resPath = safari.extension.baseURI;/' dev/js/safari-ext.$FILE_PREF.js > $FILE_PREF.safariextension/js/safari-ext.$FILE_PREF.js
test -d dev/js &&rsync -a --exclude "safari-ext.$FILE_PREF.js" dev/js/  $FILE_PREF.safariextension/js/
test -d dev/css && rsync -a dev/css/ $FILE_PREF.safariextension/css/
test -d dev/img && rsync -a dev/img/ $FILE_PREF.safariextension/img/
sed "s/###EXT_NAME###/$EXT_NAME/g" $ABSOLUTE_SCRIPT_PATH/../applescript/reload_ext.applescript | osascript
