#!/usr/bin/awk -f
#This script parses ping summary and show host status

BEGIN{
	FS = " "
}
{
	if($0 ~ /packets[[:space:]]transmitted/ ){
		if($1 > 0){
			print "up"
		}else{
			print "down"
		}
	}
}
