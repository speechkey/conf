#!/usr/bin/php
<?php
print_r($argv);

$lang = $argv[1];
$code = $argv[2];

$c = curl_init('http://markup.su/api/highlighter');
curl_setopt_array($c, array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => 'language='. $lang .'&theme=Sunburst&source=' . urlencode($code)
));
$response = curl_exec($c);
$info = curl_getinfo($c);
curl_close($c);

if ($info['http_code'] == 200 && $info['content_type'] == 'text/html') {
    echo $response;
} else {
    echo 'Error';
}
?>