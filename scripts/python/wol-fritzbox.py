#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import urllib, re, hashlib, sys, os, getopt

# Configuration (just change here) ###############################################
#host         = 'fritz.box'        # or ip (also fritz.box)
#password        = 'asdfasdf'        # your password, adapt
#protocol        = 'http'               # or http
#MAC             = '00:00:00:00:00:00'
##################################################################################

# Commands
default_login   = 'getpage=../html/de/menus/menu2.html&errorpage=../html/index.html&var:lang=de&var:pagename=home&var:menu=home&=&login:command/password=%s'
sid_challenge   = 'getpage=../html/login_sid.xml'
sid_login       = 'login:command/response=%s&getpage=../html/login_sid.xml'
wol             = 'sid=%s&getpage=../html/de/menus/menu2.html&errorpage=../html/de/menus/menu2.html&var:pagename=net&var:errorpagename=net&wakeup:settings/mac=%s'
    
# Main
def login(host, password, protocol, MAC):

    SID = ''                  # Required later                 

    try:
        # Try to get a session id SID
        sid = urllib.urlopen(protocol + '://' +  host + '/cgi-bin/webcm?' + sid_challenge)
        if sid.getcode() == 200:          
            # Read and parse the response in order to get the challenge (not a full blown xml parser)
            challenge = re.search('<Challenge>(.*?)</Challenge>', sid.read()).group(1)
            
            # Create a UTF-16LE string from challenge + '-' + password, non ISO-8859-1 characters will except here (e.g. EUR)
            challenge_bf = (challenge + '-' + password).decode('iso-8859-1').encode('utf-16le')
            
            # Calculate the MD5 hash
            m = hashlib.md5()
            m.update(challenge_bf)
    
            # Make a byte response string from challenge + '-' + md5_hex_value 
            response_bf = challenge + '-' + m.hexdigest().lower()
            
            # Answer the challenge
            login = urllib.urlopen(protocol + '://' + host + '/cgi-bin/webcm', sid_login % response_bf)

            if login.getcode() == 200:
                SID = re.search('<SID>(.*?)</SID>', login.read()).group(1)
                return SID
            else:
                print "Could not login"
                return False
    except:
        # Legacy login
        command = urllib.urlopen(protocol + '://' + host + '/cgi-bin/webcm', default_login % password)
        response = command.read()
        # Right now I don't know how to check the result of a login operation. So I just search for the errorMessage
        if command.getcode() == 200:
            try:
                result = urllib.unquote(re.search('<p class="errorMessage">(.*?)</p>', response).group(1).decode('iso-8859-1')).replace("&nbsp;"," ")
            except:
                result = ''
            print 'Login attempt was made. %s' % result
        return False

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "h", ["help", "host=", "password=", "protocol=", "mac="])
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                usage()
                sys.exit()
            elif opt in ("--host"):
                host = arg
                print host
            elif opt in ("--password"):
                password = arg
                print password
            elif opt in ("--protocol"):
                protocol = arg
                print protocol
            elif opt in ("--mac"):
                MAC = arg
                print MAC
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    SID = login(host, password, protocol, MAC)
    urllib.urlopen(protocol + '://' + host + '/cgi-bin/webcm', wol % (SID, MAC))
                                   
             

if __name__ == '__main__': 
    main(sys.argv[1:])