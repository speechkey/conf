#!/usr/bin/env bash
#
# "Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[__NSArrayM insertObject:atIndex:]: object cannot be nil'"
#
#On the line:
#
#managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles: nil] retain];


rm -rf ~/Library/Developer/Xcode/DerivedData/$1
rm -rf ~/Library/Application Support/iPhone Simulator/6.1/Applications/*
