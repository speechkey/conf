#!/usr/bin/env bash
rec -r 8000 -c 1 record_voice.wav
sox record_voice.wav message.flac pad .1 0 rate 16k
wget -q -U "Mozilla/5.0" --post-file message.flac --header="Content-Type: audio/x-flac; rate=16000" -O - "http://www.google.com/speech-api/v1/recognize?lang=ru-RU&client=chromium"
rm -rf message.flac
