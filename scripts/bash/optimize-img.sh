#!/bin/sh

############################################################ 
#
# Script for optimizing images in a directory (recursive)
# pngcrush & jpegtran settings from:
# http://developer.yahoo.com/performance/rules.html#opt_images
#
# Based on: https://gist.github.com/ryansully/1720244
#
############################################################

source $SCRIPTS_PATH/bash/dev-utils/commands-exist.sh

# check if compressors binaries available
commands_available pngcrush jpegtran

# compress .png
for png in `find $1 -iname "*.png"`; do
    echo "crushing $png ..."
    pngcrush -rem alla -reduce -brute "$png" temp.png
 
    # preserve original on error
    if [ $? = 0 ]; then
        mv -f temp.png $png
    else
        rm temp.png
    fi
done
 
# compress .jpg
for jpg in `find $1 -iname "*.jpg" -iname "*.jpeg"`; do
    echo "crushing $jpg ..."
    jpegtran -copy none -optimize -perfect "$jpg" > temp.jpg
 
    # preserve original on error
    if [ $? = 0 ]; then
        mv -f temp.jpg $jpg
    else
        rm temp.jpg
    fi
done