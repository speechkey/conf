#!/usr/bin/env bash
##################################################################################
#
# Expands relative paths like `~/Dev/`, `$SK_BIN/bash`.
#
# Usage:
#       inspect-pmd.sh <path_to_expand>
#
#		HAVE to be evaluated to the variable: DEVPATH=$(expand-path.sh ~/Dev)
#
# Author: Artem Grebenkin <speechkey@gmail.com>
# Date: 31.10.2014
#
##################################################################################

# This is part of the rules of ~-expansion. It is clearly stated in the Bash manual that this expansion is not performed when the ~ is quoted.
# http://stackoverflow.com/questions/16703624/bash-interpret-string-variable-as-file-name-path
RET=$(echo $1 | sed 's/^~/$HOME/')
# Expand path
perl -MCwd -e 'print Cwd::realpath($ARGV[0])' $RET