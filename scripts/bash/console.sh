$SK_BIN/bash/dev-utils/colorize-shell.sh \
bold '\[ERROR\]\/.*$' red '\[ERROR\]' \
bold 'ERROR:\/.*$' red 'ERROR:' \
bold '\[WARN \]\/.*$' yellow '\[WARN \]' \
bold 'WARNING:\/.*$' yellow 'WARNING:' \
bold '\[INFO \]:\/.*$' blue '\[INFO \]' \
bold 'INFO:\/.*$' blue 'INFO:' \
bold 'Caused by:\/.*$' red 'Caused by:'
