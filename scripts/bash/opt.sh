#!/bin/bash
# ./opt.sh -o nobrowse -o nosuid,nodev,nolocks,locallocks -o @-timeout=300,-ghost,allow_other@ -o automounted -o [test] asdfed:asdfasdf@ftsdf.servage.net /Volumes/Hei
# Test: 
resopt=""
readopt='getopts $opts opt; rc=$?; [ $rc$opt == 0? ] && exit 1; [ $rc == 0 ] || { shift $[OPTIND-1]; false; }'

opts=vo:
regex="^\[.*\]$"

# Enumerating options
while eval $readopt
do
	if [[ "$opt" == "o" && $OPTARG =~ $regex ]]
    	then
		resopt="$resopt "`echo $OPTARG | sed -e 's/\[\([^]]*\)\]/\1/g'`
	fi
done

# Enumerating arguments
for arg
do
    resopt="$resopt $arg"
done

curlftpfs $resopt
