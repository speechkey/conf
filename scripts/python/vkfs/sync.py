#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# мелкий демон, который синхронизует папку с музыкой с контактом
# по просьбе morfair

import time, random
import urllib, urllib2
from md5 import md5
import stat, os, errno, sys
#~ import commands
import pynotify
#~
#~ import gettext
#~ import locale
#~ _ = gettext.gettext

import vkapi

path=os.path.expanduser('~')+'/Музыка из ВКонтакте/'
if not os.path.isdir(path):
	os.makedirs(path)

CP="utf-8"
RTIME=300
TIMEOUT=5

api=vkapi.vk()
api.checkauth()
audio=vkapi.audio()
audio.api=api

playlist=None

#~ {"response":[{"aid":"60830458","owner_id":"6492","artist":"Noname","title":"Bosco",
#~ "duration":"195","url":"http:\/\/cs40.vkontakte.ru\/u06492\/audio\/2ce49d2b88.mp3"},
#~ {"aid":"59317035","owner_id":"6492","artist":"Mestre Barrao","title":"Sinhazinha",
#~ "duration":"234","url":"http:\/\/cs510.vkontakte.ru\/u2082836\/audio\/
#~ d100f76cb84e.mp3"}]}

def show_msg(text):
	"""Sends message to the notification area"""
	icon = 'vk_logo'
	title = 'Синхронизатор ВКонтакта'
	notif = pynotify.Notification(title, text, icon);
	notif.show()

while True:
	playlist=audio.get()
	try:
		playlist=playlist['response']
		playlist.reverse()
		for a in playlist:
			url  = a['url']
			name = (api.unescape(a['artist']) + ' - ' + api.unescape(a['title'])).replace('/','\\').encode(CP) + '.' + url.split('.')[-1]

			if not os.path.isfile(path + name):
				print '***saving...',name
				urllib.urlretrieve(url, path + name)
			else:
				meta = urllib2.urlopen(url,timeout=TIMEOUT).info()
				size = int(meta['Content-Length'])
				fsize = os.path.getsize(path + name)
				print '***checking...',name
				print size,fsize,(size==fsize)
				if (size!=fsize) : urllib.urlretrieve(url, path + name)
				
	except:
		print '***error',playlist
		try:
			show_msg(playlist['error']['error_msg'])
		except:
			show_msg('Неизвестная ошибка или ошибка сети')

	time.sleep(RTIME)
	audio.api.checkauth()


#~ {'error': {'error_code': 3, 'error_msg': 'Unknown method passed: Session can be expired, revoked by user or connected with different IP address', 'request_params': [{'value': 'JSON', 'key': 'format'}, {'value': '3.0', 'key': 'v'}, {'value': '2256550', 'key': 'api_id'}, {'value': 'e0d0fe8bf0a926570d315cc5b45a47a0', 'key': 'sig'}, {'value': 'fb70a89a3258835db7f7167d4dd82f903e87d18f3214c4e42187dc4b2f9e3a', 'key': 'sid'}, {'value': 'audio.get', 'key': 'method'}]}}
