on run argv
	set playlist_name to (item 1 of argv)
	set newline to "
"
	set listPlay to ""
	tell application "iTunes"
		set num_tracks to count of tracks in user playlist playlist_name
		
		repeat with c from 1 to count of tracks in user playlist playlist_name
			set track_name to (get name of track c of user playlist playlist_name)
			set track_artist to (get artist of track c of user playlist playlist_name)
			
			set listPlay to listPlay & track_name & "|" & track_artist & newline
		end repeat
	end tell
	get listPlay
end run