#!/bin/bash

CD="/Applications/CocoaDialog.app/Contents/MacOS/CocoaDialog"
ARTIST=$1
TITLE=$2

LYRIC=`php -f go.php $ARTIST $TITLE`

$CD textbox \
    --title "$ARTIST — $TITLE" \
    --text "$LYRIC" \
    --button1 "Ok"

