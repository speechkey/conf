application "System Events"
	if exists process "CharacterPalette" then
		try
			quit application "CharacterPalette"
		end try
	end if
end tell
tell application "Finder"
	open item "System:Library:Input Methods:CharacterPalette.app" of the startup disk
end tell
