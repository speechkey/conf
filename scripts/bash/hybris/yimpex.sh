#!/usr/bin/env bash

HAC_PROTOCOL="http"
HAC_URL="localhost:9001"
FILEPATH=$1

. $SK_BIN/bash/hybris_auth.sh

curl -s -H "Cookie: $YAUTH_COOKIE" -H "Content-Type: multipart/form-data;" -H "Referer: $HAC_PROTOCOL://$HAC_URL/console/impex/import/upload?_csrf=$YAUTH_CSRF" \
-F "encoding=UTF-8" \
-F "maxThreads=5" \
-F "validationEnum=IMPORT_RELAXED" \
-F "legacyMode=true" \
 -F "_legacyMode=on" \
 -F "enableCodeExecution=true" \
 -F "_enableCodeExecution=on" \
 -F "_csrf=$YAUTH_CSRF" \
 -F "file=@$FILEPATH;type=application/octet-stream" $HAC_PROTOCOL://$HAC_URL/console/impex/import/upload?_csrf=$YAUTH_CSRF \
 | sed -e '/<pre>/,/<\/pre>/!d' | gtail -n +3 |  ghead -n -2
#