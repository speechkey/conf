--Script sets rating of the currently played song in iTunes to the first argunment
--Example: osascript itunes-rating.scpt 4 - Set curruntly played track rating to 4
 
on run argv
	tell application "iTunes"
		if player state is playing then
			set theTrack to the current track
			set the rating of theTrack to item 1 of argv * 20
		end if
	end tell
end run
