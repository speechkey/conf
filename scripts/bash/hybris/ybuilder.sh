
CONTAINS_DELETED_OR_RENAMED=`git --git-dir=$1/.git --work-tree=$1 status -s | awk '{print $1}' | grep -e "D" -e "R" | wc -l`

if [ $CONTAINS_DELETED_OR_RENAMED -gt 0 ]; then
    ant clean all -find $1 > $2
else
    ant -find $1 > $2
fi

