#!/bin/bash

PLIST_PATH=$SK_HOME/services/launchd/LaunchDaemons

service_status_silent (){
        DAEMON_FOUND=`sudo launchctl list | grep $DAEMON_NAME | wc -l`
        if [ $DAEMON_FOUND -gt 0 ]
        then
                SERVICE_STATUS=1
        else
                SERVICE_STATUS=0
	fi
}
service_status (){
	DAEMON_FOUND=`sudo launchctl list | grep $DAEMON_NAME | wc -l`
	if [ $DAEMON_FOUND -gt 0 ]
	then
		 echo "$SERVICE_NAME: is up and running."
	else
		 echo "$SERVICE_NAME: is down."
	fi
}
handle_service (){
	DAEMON_FOUND=`sudo launchctl list | grep $DAEMON_NAME | wc -l`
	if [ "$SERVICE_ACTION" == "start" ] && [ $DAEMON_FOUND -eq 0 ]
	then
        	startup_output=`sudo launchctl load -w $PLIST`
	elif [ "$SERVICE_ACTION" == "start" ] && [ $DAEMON_FOUND -gt 0 ]
	then
		echo "$SERVICE_NAME: is already running."
		exit 0
	elif [ "$SERVICE_ACTION" == "stop" ] && [ $DAEMON_FOUND -gt 0 ]
	then
	        shutdown_output=`sudo launchctl unload -w $PLIST`
	elif [ "$SERVICE_ACTION" == "stop" ] && [ $DAEMON_FOUND -eq 0 ]
	then
		echo "$SERVICE_NAME: is already down."
		exit 0
	elif [ "$SERVICE_ACTION" == "restart" ] && [ $DAEMON_FOUND -gt 0 ]
	then
	        shutdown_output=`sudo launchctl unload -w $PLIST`
		service_status_silent
		while [ $SERVICE_STATUS == "1" ]
		do
			sleep 1
			service_status_silent
		done
		service_status
	        shutdown_output=`sudo launchctl load -w $PLIST`
	elif [ "$SERVICE_ACTION" == "restart" ] && [ $DAEMON_FOUND -eq 0 ]
	then
	        shutdown_output=`sudo launchctl load -w $PLIST`
	else
		true
	fi
	sleep 1
	service_status
}

SERVICE_ACTION=$2

if [ "$1" == "mysql" ]
then
	SERVICE_NAME='MySQL'
	DAEMON_NAME='mysql'
	PLIST=$PLIST_PATH/homebrew.mxcl.mysql.plist
	handle_service
elif [ "$1" == "mongo" ]
then
	SERVICE_NAME='MongoDB'
	DAEMON_NAME='mongo'
	PLIST=$PLIST_PATH/homebrew.mxcl.mongodb.plist
	handle_service
elif [ "$1" == "apache" ]
then
	SERVICE_NAME='Apache'
	DAEMON_NAME='apache'
	PLIST=$PLIST_PATH/org.apache.httpd.plist
	handle_service
elif [ "$1" == "nginx" ]
then
	SERVICE_NAME='Nginx'
	DAEMON_NAME='nginx'
	PLIST=$PLIST_PATH/net.gremoz.nginx.plist
	handle_service
elif [ "$1" == "php-fpm-53" ]
then
	SERVICE_NAME='php-fpm-53'
	DAEMON_NAME='php53'
	PLIST=$PLIST_PATH/homebrew-php.josegonzalez.php53.plist
	handle_service
elif [ "$1" == "php-fpm-54" ]
then
	SERVICE_NAME='php-fpm-54'
	DAEMON_NAME='php54'
	PLIST=$PLIST_PATH/homebrew-php.josegonzalez.php54.plist
	handle_service
elif [ "$1" == "postfix" ]
then
	SERVICE_NAME='Postfix'
	DAEMON_NAME='postfix'
	PLIST=$PLIST_PATH/org.speechkey.postfix.plist
	handle_service
elif [ "$1" == "jenkins" ]
then
	SERVICE_NAME='Jenkins'
	DAEMON_NAME='jenkins'
	PLIST=$PLIST_PATH/net.gremoz.jenkins-ci.plist
	handle_service
elif [ "$1" == "redis" ]
then
	SERVICE_NAME='Redis'
	DAEMON_NAME='redis'
	PLIST=$PLIST_PATH/homebrew.mxcl.redis.plist
	handle_service
elif [ "$1" == "elasticsearch" ]
then
	SERVICE_NAME='ES'
	DAEMON_NAME='elasticsearch'
	PLIST=$PLIST_PATH/homebrew.mxcl.elasticsearch.plist
	handle_service
elif [ "$1" == "elasticsearch0909" ]
then
	SERVICE_NAME='ES v0.90.9'
	DAEMON_NAME='elasticsearch'
	PLIST=$PLIST_PATH/homebrew.mxcl.elasticsearch0909.plist
	handle_service
elif [ "$1" == "status" ]
then
	SERVICES='ES v0.90.9;elasticsearch|ES;elasticsearch|Apache;apache|MongoDB;mongo|MySQL;mysql|Nginx;nginx|php-fpm-53;php-fpm-53|php-fpm-54;php-fpm-54|Postfix;postfix'
	for i in $(echo $SERVICES | tr "|" "\n")
	do
		SERVICE_NAME=${i%;*}
		DAEMON_NAME=${i#*;}
		service_status
	done
else
	echo "Usage: iservice.sh status | <service> <action>"
	echo "status: show information about all services"
	echo "Supported services:"
	echo "  nginx"
	echo "Supported actions:"
	echo "  start"
	echo "  stop"
	echo "  status"
fi
