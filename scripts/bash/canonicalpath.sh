#!/bin/bash
#
# Resolves a full path
#
# Copyright: http://stackoverflow.com/questions/1245301/canonicalize-a-path-name-on-solaris

canonicalpath() {
  if [ -d $1 ]; then
    pushd $1 > /dev/null 2>&1
    echo $PWD
  elif [ -f $1 ]; then
    pushd $(dirname $1) > /dev/null 2>&1
    echo $PWD/$(basename $1)
  else
    echo "Invalid path $1"
  fi
  popd > /dev/null 2>&1
}
