#!/usr/bin/env bash
##################################################################################
#
# Checks java files on checkstyle issues with given ruleset. Is able to print
# execution log to stdout. Exits with 1 if checkstyle issues was found or with 1
# if not.
#
# Usage:
#       inspect-checkstyle.sh --silent --properties ~/checkstyle.properties --level warning --ruleset ~/checkstyle-ruleset.xml ~/TestClass.java ~/TestClass2.java
#
#            --ruleset: Path to the checkstyle ruleset
#            --silent: Optional. Don't log checkstyle output
#            --level: Optional. Checkstyle notification level to fail, default `warning`
#            --properties: Optional. Path to the checkstyle properies file
#
# Author: Artem Grebenkin <speechkey@gmail.com>
# Date: 09.10.2014
#
##################################################################################

# For using scripts in dev-utils
PATH=$PATH:$SK_BIN/bash/dev-utils
# Check dependencies
check-cmd-exists.sh checkstyle || exit 1

checkstyle -p checkstyle.properties -c ~/Dev/xplace/repo/scripts/coding_conventions/rulesets/checkstyle.xml /Users/agrebenkin/Dev/xplace/repo/hybris/bin/custom/slstorefront/web/src/de/simply/local/slstorefront/web/brand/CMSBrandListComponentController.java
