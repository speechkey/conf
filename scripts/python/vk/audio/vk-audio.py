#!/usr/bin/env python
# -- coding: utf-8 --
#
# Download tracks from vk.com playlist. Already downloaded tracks, don't fetched next time.
# Tracks log stored in sqlite db, so if they get relocated, they will be not fetched again.
# Script can also only print tracks information and avoid downloading them.
#
#TODO: Get/store credentials from/in keychain, access token or vk.com users password.
#TODO: Download a few playlists.
#TODO: Verify certificate to avoid mitm attack http://thejosephturner.com/blog/2011/03/19/https-certificate-verification-in-python-with-urllib2/
#FIXME: Gets an error by downloading track: Artist: Radio Record, Title: DJ Antoine feat. Houseshakers - Ma Cherie (Electro Elephants Sax Mix) (Radio Record) http://www.radiorecord.ru/.mp3
#Process Process-1:
#Traceback (most recent call last):
#  File "/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/multiprocessing/process.py", line 258, in _bootstrap
#    self.run()
#  File "/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/multiprocessing/process.py", line 114, in run
#    self._target(*self._args, **self._kwargs)
#  File "./vk-audio.py", line 83, in worker
#    FILE = open(abs_file_path, 'w')
#IOError: [Errno 2] No such file or directory: u'/Users/speechkey/Downloads/VK/Playlists/Sync/Radio Record - DJ Antoine feat. Houseshakers - Ma Cherie (Electro Elephants Sax Mix) (Radio Record) http://www.radiorecord.ru/.mp3'


import vkontakte
import sqlite3
import datetime
from multiprocessing import Process, Queue
import os
import urllib
import argparse


def createTracksDB(path):
    '''
    Create a database, where all fetched tracks are logged.

    Keyword arguments:
    path -- File system pyth to the db.

    Return -- Operation status (boolean).
    '''
    try:
        conn = sqlite3.connect(path)
        c = conn.cursor()
        c.execute('''CREATE TABLE songs (artist TEXT, title TEXT, tmpstamp INTEGER)''')
        conn.commit()
        conn.close()
        return True
    except sqlite3.Error, e:
        print "Error occured while creating a db: " + e
        return False


def persistLog(conn, track):
    '''
    Log successfully downloaded track to the db.

    Keyword arguments:
    conn -- sqlite3 connection object (object)
    track -- track inforamtion will be logged (dict{'artist': '','title': ''})

    Return -- Operation status (boolean).
    '''
    try:
        c = conn.cursor()
        c.execute('INSERT INTO songs VALUES (?, ?, ?)', [track['artist'], track['title'], datetime.datetime.now()])
        conn.commit()
        return True
    except sqlite3.Error, e:
        print "Unable to persist a log: " + e
        return False


def worker(queue, path, conn):
    '''
    Worker each executed in separete process. It pops a track url, downloads the track and log
    it to the db.

    Keyword arguments:
    queue -- tracks queue (dict {'url': '', 'artist': '', 'title': ''})
    path -- file system path to the playlist folder (string)
    conn -- sqlite3 connection object (object)
    '''
    #Get next tracks url from queue and process it if not a 'STOP', which is end of the queue
    for track in iter(queue.get, 'STOP'):
        #create folder for a playlist if not exists
        abs_dir_path = os.path.expanduser(path)
        if not os.path.exists(abs_dir_path):
            os.makedirs(abs_dir_path)

        abs_file_path = abs_dir_path + "/" + track['artist'] + " - " + track['title'] + ".mp3"
        #If file not already exists, download it
        if not os.path.exists(abs_file_path):
            URL = urllib.urlopen(track['url'])
            FILE = open(abs_file_path, 'w')
            FILE.write(URL.read())
            URL.close()
            FILE.close()
            print "Downloaded: %s" % abs_file_path
        #Log track to the db
        if os.path.exists(abs_file_path):
            persistLog(conn, track)


def main():
    #Get script args
    script_args = args()
    #Autorize app if no token
    if not script_args.ACCESS_TOKEN:
        import getpass
        from vkappauth import VKAppAuth

        password = getpass.getpass("VK.com users password:")

        try:
            #Authorize application
            vk_auth = VKAppAuth()
            access_data = vk_auth.auth(script_args.VK_EMAIL, password, script_args.VK_APP_ID, 'audio,offline')
            script_args.ACCESS_TOKEN = access_data['access_token']
        except Exception, message:
            print message
            exit()
    #Create api wrapper
    vk = vkontakte.API(token=script_args.ACCESS_TOKEN)

    #Get users or groups playlists
    try:
        playlistsJson = vk.audio.getAlbums(gid=script_args.GROUP_ID, uid=script_args.USER_ID)
    except Exception, data:
        print "Unable to query api: " + data.description
        exit()
    #Filter not playlists out
    playlists = (playlist for playlist in playlistsJson if isinstance(playlist, dict))
    #Print playlists titles and exit
    if(script_args.SHOW_PLAYLISTS):
        for playlist in playlists:
            print playlist['title'].encode('utf-8') + ",",
        exit()

    if playlists:
        if not script_args.VK_PLAYLIST_TITLE:
            #TODO: how to add by argparse, if one option set, relative should be also set. For example if --gid or --uid set and --show-playlist not set --playlist should be set.
            print("Error: Which playlist would you like to syncronze? No playlist given.")
            exit()
        #TODO: To avoid this loop, store album_id somewere or allow input as a parameter. API can fetch tracks only by known album_id not album title.
        for playlist in playlists:
            #Get track if its our playlist
            if playlist['title'].encode('utf-8') == script_args.VK_PLAYLIST_TITLE:
                tracksJson = vk.audio.get(gid=script_args.GROUP_ID, album_id=playlist['album_id'])
                break
    else:
        print "Info: No audio playlists found for current user."
        exit()
    #Expand to the absolute path
    log_db_path = os.path.abspath(script_args.LOG_DB_PATH)
    #Create logging db if not exists
    if not os.path.exists(log_db_path):
        createTracksDB(log_db_path)

    conn = sqlite3.connect(log_db_path)
    c = conn.cursor()

    tracks_queue = Queue()
    #Add only not fetched tracks to the queue
    for track in tracksJson:
        c.execute("SELECT * FROM songs WHERE artist = ? AND title = ?", [track['artist'], track['title']])
        if not c.fetchone():
            #TODO: From api coming links to the not secured site, but I can change it to secure: http://cs4990.vkontakte.ru/u13363064/audio/7c1fb39dc000.mp3 -> https://ps.userapi.com/c4990/u13363064/audio/7c1fb39dc000.mp3
            if not script_args.URL_ONLY:
                tracks_queue.put({'url': track['url'], 'artist': track['artist'], 'title': track['title']})
            else:
                #Print only tracks data
                print "Artist: " + track['artist'] + " | Title: " + track['title'] + " | Url: " + track['url']
                #TODO: Do I want to log printed tracks? Hmm...
                persistLog(conn, track)

    #Start worker processes
    for i in range(script_args.NUMBER_OF_THREADS):
        Process(target=worker, args=(tracks_queue, script_args.DOWNLOAD_FOLDER, conn)).start()

    #Adding queue end label for child process
    for i in range(script_args.NUMBER_OF_THREADS):
        tracks_queue.put('STOP')

    conn.close()


def args():
    parser = argparse.ArgumentParser(description='Download tracks from vk.com playlist.')

    parser.add_argument(
        '-l', '--log-db-path',
        dest='LOG_DB_PATH', default='./vk-playlists.db',
        help='Path to the sqlite database, which contains information about already fetched tracks.\
              By default database will be created in current folder if not already exists.')

    parser.add_argument(
        '-t', '--threads',
        dest='NUMBER_OF_THREADS', default=4, type=int,
        help='Number of thread processing download. By defauth 4 threads will be spawned.')

    parser.add_argument(
        '-i', '--vk-app-id',
        dest='VK_APP_ID', type=int, required=True,
        help='ID of an application registred on vk.com.')

    parser.add_argument(
        '-g', '--gid',
        dest='GROUP_ID', default=False,
        help='Group ID, if playlist belongs to a group.')

    parser.add_argument(
        '-u', '--uid',
        dest='USER_ID', default=False,
        help='Users ID, if playlist belongs to a user.')

    parser.add_argument(
        '--show-playlists',
        dest='SHOW_PLAYLISTS', default=False, action='store_true',
        help='Show all users or groups playlists.')

    parser.add_argument(
        '--playlist',
        dest='VK_PLAYLIST_TITLE', default=False,
        help='Download playlist by title.')

    parser.add_argument(
        '--url-only',
        dest='URL_ONLY', default=False, action='store_true',
        help='Print only track urls: Artist: <artist> | Title: <title> | Url: <url>')

    parser.add_argument(
        '-e', '--vk-email',
        dest='VK_EMAIL', required=True,
        help='Email of your vk.com user.')

    parser.add_argument(
        '-a', '--access-token',
        dest='ACCESS_TOKEN', default=False,
        help="Don't try to autorize, use existing access token.")

    parser.add_argument(
        '--download-folder',
        dest='DOWNLOAD_FOLDER', default='./',
        help='Download folder for mp3 files. By default files stored in current folder.')

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
