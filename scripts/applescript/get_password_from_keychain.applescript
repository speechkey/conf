-- Get password from system keychain by name of the item.
--
-- @param itemName Name of the item with password
-- @return Items password stored in the keychain
--
-- Usage:
-- 		set passwd to getPasswordFromKeychain("Alfred.app")

on getPasswordFromKeychain(keychainItemName)
	do shell script "security 2>&1 >/dev/null find-generic-password -gl " & quoted form of keychainItemName & " | awk '{print $2}'"
	return (text 2 thru -2 of result)
end getPasswordFromKeychain