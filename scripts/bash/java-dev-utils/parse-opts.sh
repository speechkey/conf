

# Usage:
#       parse-opt.sh -o,--optional,--required:,-r:

PATH=$PATH:$SK_BIN/bash/dev-utils

PATTERN=$1
shift 2
PATTERN_ARGS=$@

LONG_OPTS_ARG=""
LONG_OPTS_PATTERN=^--[a-z0-9]+\-?[a-z0-9]+\:?$

SHORT_OPTS_ARG=""
SHORT_OPTS_PATTERN=^-[a-z]\:?$
# Set bash field separator to `,` to be able to iterate over options
IFS=','

for OPT in $PATTERN; do
    # check if all options patterns are valid
    if [[ $OPT =~ $LONG_OPTS_PATTERN ]]; then
        [[ "$LONG_OPTS_ARG" == "" ]] && LONG_OPTS_ARG=$LONG_OPTS_ARG${OPT:2} || LONG_OPTS_ARG=$LONG_OPTS_ARG,${OPT:2}
    elif [[ $OPT =~ $SHORT_OPTS_PATTERN ]]; then
        [[ "$SHORT_OPTS_ARG" == "" ]] && SHORT_OPTS_ARG=$SHORT_OPTS_ARG${OPT:1} || SHORT_OPTS_ARG=$SHORT_OPTS_ARG,${OPT:1}
    else
        echo "Error: $OPT is invalid option format." | colorize-shell.sh red "Error: "
        exit 1
    fi
done

IFS=' '

PARSER_RESULT=$(/usr/local/Cellar/gnu-getopt/1.1.5/bin/getopt -o $SHORT_OPTS_ARG --longoptions $LONG_OPTS_ARG -- $PATTERN_ARGS)

eval set -- "$PARSER_RESULT"

while true
do
    [[ "$1" == "--" ]] && break

    ( [[ "$1:" =~ $LONG_OPTS_ARG ]] || [[ "$1:" =~ $SHORT_OPTS_ARG ]] ) && SHIFT=2 || SHIFT=1
    [[ $1 =~ $LONG_OPTS_PATTERN ]] && ARG_NAME=${1:2} || ARG_NAME=${1:1}
    if [ $SHIFT -eq 1 ]; then
        echo "1"
        shift
    fi
    if [ $SHIFT == 2 ]; then
        declare -x "$ARG_NAME=$2"
        shift 2
    fi

done

