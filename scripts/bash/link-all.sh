# Create links in current folder for every directory in target folder
# ./link-all.sh (file|dir) <path>

IFS=$'\n'

#Check for what kind of elements to create links 
if [ "$1" == "files" ];
then
	type='f'
else
	type='d'
fi

for file in `find $2 -type $type -depth 1`;
do
	ln -s $file
done

unset $IFS
