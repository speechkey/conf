#################
# Use for cron jobs to be notified with growl about updates in origin repository.
#
# Usage:
#        git-fetch-notifier.sh <git-repo-path> <growl-message>
#
################

FETCH_STATUS=`git -C $1 fetch | grep "Total \d" | wc -l`
if [ $FETCH_STATUS -eq 1 ]; then
	growlnotify -m $2
	echo "Fetch"
fi
