#!/bin/bash
#This script open vkontakte.ru audio search for current played track on radio record

PWD=`dirname $0`
TRACK=`curl -s http://www.radiorecord.ru/script/record_pres.txt | $PWD/../awk/parse_track_name.awk | tr ' ' '\ '`
#Encode track title
TRACK_ENCODED=$(echo -n "$TRACK" | perl -pe's/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg');
URL="https://vk.com/search?c[q]=$TRACK_ENCODED&c[section]=audio"
open "$URL"
