#!/usr/bin/env bash
#################################################
## Install configuration
## Author: Artem Grebenkin <speechkey@gmail.com>
## Date: 06.10.2014
#####

##
# Checks if file exists and moves them on demand to the backup file with name <filename>.bak.
#
# Example: backup_file ~/.local-env-vars
# 
# $1 File path
#
function backup_file(){
	[[ -f $1 ]] &&
	while true; do
	    read -p "File \`$1\` already exists. Do you wish to backup it?" yn
	    case $yn in
	        [Yy]* ) mv $1 $1.bak; break;;
	        [Nn]* ) rm -rf $1; break;;
	        * ) echo "Please answer \`y\` or \`n\`.";;
	    esac
	done
}
#######
# Generate file which configure environment variables to be used in terminals and GUI applications.
#######

# Default path to the file with environment
ENV_VARS_FILE_PATH=~/.local-env-vars

# Build path for the repostiry
USERNAME=$(whoami)
HOSTNAME=$(hostname)
SK_HOME_DIR_NAME="~/.$USERNAME@$HOSTNAME"

# Offer user to input deviant path for the repository
read -p "Enter directory where you checked out the repostiry [$SK_HOME_DIR_NAME]:" USER_SK_HOME_DIR_NAME
# Prefer the path inputed by user
! [[ "$USER_SK_HOME_DIR_NAME" == "" ]] && SK_HOME_DIR_NAME=$USER_SK_HOME_DIR_NAME

if [ "$(uname)" == "Darwin" ]; then
    SHELL_ENV_SUFIX="mac"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    SHELL_ENV_SUFIX="linux"
fi

# Check if file already exists to be able to break
if [ ! -f $ENV_VARS_FILE_PATH ]; then
	touch $ENV_VARS_FILE_PATH
else
	backup_file $ENV_VARS_FILE_PATH
fi

# Generate configuration
cat > $ENV_VARS_FILE_PATH <<EOL
#!/usr/bin/env bash
##################################################################
# Contains user defined shell environment variables for a certain
# host and username. All hosts specific environment variable should be
# added to this file. Have to be linked under ~/.local-env-vars.
#
# WARNING: this file should contain only \`export\` or \`source\`
# commands to be able to be processed by an environment exporter agent
# \`net.gremoz.env-vars.plist\` which export environment variables
# to Mac Os UI apps.
#
# Author: Install Script <install.sh>
# Date: $(date +"%m.%d.%Y")
#
##################################################################

################################################
# DON'T REMOVE! Configure system.
export SK_HOME=${SK_HOME_DIR_NAME}
export SK_CONF=\$SK_HOME/conf
export SK_BIN=\$SK_HOME/scripts

# Setup host independent enviroment variables. 
source \$SK_CONF/shell_env.$SHELL_ENV_SUFIX
################################################

################################################
# To override default environment variables 
# insert them below...

EOL

####
# Link zsh config
###
echo "Linking zshrc..."

ZSHRC_PATH=~/.zshrc
backup_file $ZSHRC_PATH

# Generate configuration
cat > $ZSHRC_PATH <<EOL
#!/usr/bin/env zsh
##################################################################
# Setup zsh environment on the workstation.
#
# Author: Install Script <install.sh>
# Date: $(date +"%m.%d.%Y")
#
##################################################################

################################################
# DON'T REMOVE! Configure system.
source ~/.local-env-vars
# Setup host independent zsh configuration
source \$SK_CONF/zshrc.$SHELL_ENV_SUFIX
################################################

################################################
# To override default zsh configuration
# insert below...

EOL

####
# Link bashrc config
###
echo "Linking bashrc..."

BASHRC_PATH=~/.bashrc
backup_file $BASHRC_PATH

cat > $BASHRC_PATH <<EOL
#!/usr/bin/env bash
##################################################################
# Setup bash environment on the workstation.
#
# Author: Install Script <install.sh>
# Date: $(date +"%m.%d.%Y")
#
##################################################################

################################################
# DON'T REMOVE! Configure system.
source ~/.local-env-vars
# Setup host independent bash configuration
source \$SK_CONF/bashrc.$SHELL_ENV_SUFIX
################################################

################################################
# To override default bash configuration
# insert below...

EOL

# Get environment variables
source $ENV_VARS_FILE_PATH

####
# Link vim config
###
echo "Linking vimrc..."

VIMRC_PATH=~/.vimrc
backup_file $VIMRC_PATH

cat > $VIMRC_PATH <<EOL
" #!/usr/bin/env bash
" ##################################################################
" # Setup vim environment on the workstation.
" #
" # Author: Install Script <install.sh>
" # Date: $(date +"%m.%d.%Y")
" #
" ##################################################################

" ################################################
" # DON'T REMOVE! Setup host independent vim configuration
source \$SK_CONF/vimrc
" ################################################

" ################################################
" # To override default vim configuration
" # insert below...

EOL

####
# Link inputrc config
###
echo "Linking inputrc..."

INPUTRC_PATH=~/.inputrc
backup_file $INPUTRC_PATH

cat > $INPUTRC_PATH <<EOL
#!/usr/bin/env bash
##################################################################
# Setup inputrc environment on the workstation.
#
# Author: Install Script <install.sh>
# Date: $(date +"%m.%d.%Y")
#
##################################################################

################################################
# DON'T REMOVE! Setup host independent inputrc configuration
source \$SK_CONF/inputrc
################################################

################################################
# To override default inputrc configuration
# insert below...

EOL
