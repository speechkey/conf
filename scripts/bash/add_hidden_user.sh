#!/bin/bash
#In MacOS-X you can hide users with a user-id below 500, so I had to check which number below the 500 was free.
#Using the following commands I got a list of all IDs for the users and the groups and found, that – at least on my system – everything from 300 through 399 was free, so I will settle for the 300 in this example.
# List all User-IDs
#dscl . list /Users UniqueID | sed -E "s/ +/ /g" | cut -d " " -f 2 | sort
# List all Group-IDs
#dscl . list /Groups PrimaryGroupID | sed -E "s/ +/ /g" | cut -d " " -f 2 | sort

# Configure these
USERNAME="redis"
UIDNUMBER=300
GIDNUMBER=300
# Check if entries already exist

USERNAME_EXISTS=`dscl . readall /users | grep "RecordName: $USERNAME" | wc -l`
GROUPNAME_EXISTS=`dscl . readall /groups | grep "RecordName: $USERNAME" | wc -l`
if [ $USERNAME_EXISTS == "1" ]; then
	echo "Error: User with a name $USERNAME already exists."
	exit 1
elif [ $GROUPNAME_EXISTS == "1" ]; then
	echo "Error: Group with a name $USERNAME already exists."
	exit 1
fi

UIDNUMBER_EXISTS=`dscl . readall /users | grep "UniqueID: $UIDNUMBER" | wc -l`
if [ $UIDNUMBER_EXISTS == "1" ]; then
	echo "Error: User with the UniqueID $UIDNUMBER already exists."
	exit 1
fi

GIDNUMBER_EXISTS=`dscl . readall /groups | grep "PrimaryGroupID: $GIDNUMBER" | wc -l`
if [ $GIDNUMBER_EXISTS == "1" ]; then
	echo "Error: Group with the PrimaryGroupID $GIDNUMBER already exists."
	exit 1
fi

# Create the group
sudo dscl . create /Groups/$USERNAME
sudo dscl . create /Groups/$USERNAME PrimaryGroupID $GIDNUMBER
# Create the user
sudo dscl . create /Users/$USERNAME
sudo dscl . create /Users/$USERNAME PrimaryGroupID $GIDNUMBER
sudo dscl . create /Users/$USERNAME UniqueID $UIDNUMBER
sudo dscl . create /Users/$USERNAME UserShell /usr/bin/false
# Add the user to the group
sudo dscl . append /Groups/$USERNAME GroupMembership $USERNAME
# Add users folder
sudo dscl . -create /Users/$USERNAME NFSHomeDirectory /var/empty
