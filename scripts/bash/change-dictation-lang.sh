#!/bin/bash
k="com.apple.speech.recognition.AppleSpeechRecognition.prefs DictationIMLocaleIdentifier"
if [[ "$1" == "de" ]]; then
  defaults write $k de-DE
  defaults write com.apple.assistant "Session Language" de-DE
else
  defaults write $k en-US 
  defaults write com.apple.assistant "Session Language" en-US
fi
killall -HUP DictationIM
