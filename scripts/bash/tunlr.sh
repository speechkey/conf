#! /usr/bin/env bash
###
# me.sh
#
# Updates Pandora IP in /etc/hosts file. Real Pandora IPs are provided by http://tunlr.net
# Script check if IP has changed and updates it in /etc/hosts.
#
# @permisions sudo
#
# Created by Artem Grebenkin.
# Copyright (c) 2013 Artem Grebenkin <artem.grebenkin@gmail.com>. All rights reserved.
#
###
# indicates if changes has been done
STATUS=0
# real pandora IPs
HOSTS="www.hulu.com www.cbs.com www.pandora.com tuner.pandora.com t1-1.p-cdn.com cont-1.p-cdn.com mediaserver-sv5-rt-1.pandora.com"
TUNLR_IP="192.95.16.109"
for HOST in $HOSTS
do
	CURRENT_IP=`grep $HOST /etc/hosts | sed -e 's/(\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}) $HOST/\1/' | cut -d ' ' -f 1`
	NEW_IP=`dig $HOST @$TUNLR_IP +short | sed -e '/\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}/!d' | sed -n 1p`
	if [ "$CURRENT_IP" != "$NEW_IP" ]
	then
		echo "sed -e \"s/$CURRENT_IP $HOST/$NEW_IP $HOST/\" /etc/hosts"
		STATUS=1
	fi
done

# Remove sed backup files sed has processed. See http://blog.remibergsma.com/2012/09/18/sed-inline-editing-different-on-mac-osx/
if [ $STATUS -eq 1 ]
then
#	rm -rf /etc/hosts.bak
	echo "Pandora IP has been changed."
fi
