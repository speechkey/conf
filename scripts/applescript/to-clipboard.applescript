--Script puts first argument gets from command line to the clipboard.
--Example: osascript to-clipboard.applescript 'Hello world!!!' -> the test it by Cmd+V

on run argv
	tell application "Finder" to set the clipboard to the item 1 of argv as text
end run
