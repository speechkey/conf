#!/bin/bash
#This script displays current played track on radio record
PWD=`dirname $0`
TRACK=`curl -s http://www.radiorecord.ru/script/record_pres.txt | $PWD/../awk/parse_track.awk`
growlnotify --image $PWD/../artefacts/radio-record.jpg -m "$TRACK"
