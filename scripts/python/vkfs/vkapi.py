#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import httplib
import urllib
import pycurl

from hashlib import md5

from simplejson import dumps as to_json
from simplejson import loads as from_json
import cPickle

import auth
api_id=2256550

class Buffer():
	def __init__(self):
		self.data=''
	def body_callback(self, buf):
		self.data = self.data + buf

class status():
	api = None
	text = ''
	def get(self,uid=None):
		
		query = {'method':'status.get'}
		if uid != None: query['uid']=uid
		infos = self.api.gettree(query)
		print '*** status',infos
		try:
			infos = infos['response']
			self.text = infos['text']
			return self.api.unescape(infos['text'])
		except:
			return -1

	def set(self,text):
		query = {'method':'status.set','text':text}
		infos = self.api.gettree(query)
		print '*** status',infos
		try:
			infos = infos['response']
			if infos == 1: self.text = text
			return infos
		except:
			return -1

class subscriptions():
	api = None
	def get(self,uid=None,offset=0,count=100):
		query = {'method':'subscriptions.get','offset':offset,'count':count}
		if uid != None:
			query['uid'] = uid
		infos = self.api.gettree(query)
		print '*** subscriptions.get',infos
		try:
			infos = infos['response']
			return infos
		except:
			return {'count':0,'users':[]}
			
	def getFollowers(self,uid=None,offset=0,count=100):
		query = {'method':'subscriptions.getFollowers','offset':offset,'count':count}
		if uid != None:
			query['uid'] = uid
		infos = self.api.gettree(query)
		print '*** subscriptions.getFollowers',infos
		try:
			infos = infos['response']
			return infos
		except:
			return {'count':0,'users':[]}

class photos():
	api = None
	text = ''
	def createAlbum(self,title,privacy=None,comment_privacy=None,description=None):
		query = {'method':'photos.createAlbum','title':title}
		if privacy != None:
			query['privacy'] = privacy
		if comment_privacy != None:
			query['comment_privacy'] = comment_privacy
		if description != None:
			query['description'] = description
		infos = self.api.gettree(query)
		try:
			infos = infos['response']
			return infos
		except:
			return -1
			
	def editAlbum(self,aid,title,privacy=None,comment_privacy=None,description=None):
		query = {'method':'photos.editAlbum','aid':aid,'title':title}
		if privacy != None:
			query['privacy'] = privacy
		if comment_privacy != None:
			query['comment_privacy'] = comment_privacy
		if description != None:
			query['description'] = description
		infos = self.api.gettree(query)
		try:
			infos = infos['response']
			return infos
		except:
			return -1
			
	def getUploadServer(self,aid,gid=None):
		query={'method': 'photos.getUploadServer', 'aid':aid}
		if gid != None : query['gid'] = gid
		print '*** photos.getUploadServer:' , query
		UploadServer = self.api.gettree(query)
		print '*** ', UploadServer
		try:
			return UploadServer['response']['upload_url']
		except:
			return -1

class audio():
	api=None
	def getUploadServer(self,gid=None):
		query={'method': 'audio.getUploadServer'}
		if gid != None : query['gid'] = gid
		print '*** audio.getUploadServer:' , query
		UploadServer = self.api.gettree(query)
		print '*** ', UploadServer
		try:
			return UploadServer['response']['upload_url']
		except:
			return -1
	def save(self,server,audio,hash):
		query={'server':server,'audio':audio,'hash':hash,'method':'audio.save'}
		result=self.api.gettree(query)
		return result
	def get(self,uid=None,gid=None,aids=None,need_user=None):
		query={'method':'audio.get'}
		if uid != None : query['uid']=uid
		if gid != None : query['gid']=gid
		if aids != None : query['aids']=aids
		if need_user != None : query['need_user']=need_user
		result=self.api.gettree(query)
		return result
		
class docs:
	api=None
	def get(self,uid=None,count=None,offset=None):
		query={'method':'docs.get'}
		if uid != None : query['uid']=uid
		if count != None : query['count']=count
		if offset != None : query['offset']=offset
		result=self.api.gettree(query)
		return result
	
				

class vk:
	session=None
	def unescape(self, s):
		s = s.replace("&lt;", "<")
		s = s.replace("&gt;", ">")
		s = s.replace("&apos", "'")
		s = s.replace("&quot;", "\"")
		s = s.replace("&amp;", "&")
		s = s.replace("&#39;", "'")
		
		return s
		
	def checkauth(self):
		try:
			self.session=auth.load()['session']
			self.mid = self.session['mid']
		except:
			return -1
		settings = self.gettree({'method':'getUserSettings'})
		try:
			self.settings = settings['response']
			return self.settings
		except:
			return -1
		# print self.session

	def gettree(self,query):
		query['format'] = 'JSON'
		try:
			return from_json(self.get(query))
		except:
			return {}
	def get(self,query):
		api = httplib.HTTPConnection("api.vkontakte.ru")
		sid = self.session["sid"]
		mid = self.session["mid"]
		secret = self.session["secret"]
		query['api_id'] = api_id
		query['v'] = "3.0"
		sortedquery = ''
		for i in sorted(query.keys()):
			sortedquery = sortedquery + i + '=' + str(query[i])
		sig = md5(str(mid)+sortedquery+secret).hexdigest()
		query['sig'] = sig
		query['sid'] = sid
		params = urllib.urlencode(query)
		api.request("POST", "/api.php", params, {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"})
		response = api.getresponse()
		data = response.read()
		api.close()
		print '**** respond', data
		return data

	def getlink(self,query):
		sid = self.session["sid"]
		mid = self.session["mid"]
		secret = self.session["secret"]
		query['api_id'] = api_id
		query['v'] = "3.0"
		sortedquery = ''
		for i in sorted(query.keys()):
			sortedquery = sortedquery + i + '=' + str(query[i])
		sig = md5(str(mid)+sortedquery+secret).hexdigest()
		query['sig'] = sig
		query['sid'] = sid
		params = urllib.urlencode(query)
		link = 'http://api.vkontakte.ru/api.php?' + params
		return link
		
	def upload(self,server,fieldname,filename):
		print '*** upload',server,fieldname,filename
		bf= Buffer()
		c = pycurl.Curl()
		c.setopt(c.POST, 1)
		c.setopt(c.URL, server)
		c.setopt(c.HTTPPOST, [(fieldname, (c.FORM_FILE, filename))])
		c.setopt(c.WRITEFUNCTION, bf.body_callback)
		c.setopt(c.VERBOSE, 1)
		c.perform()
		c.close()
		#print '*** upload result',bf.data.replace(chr(1),'%01')
		return from_json(bf.data.replace(chr(1),'%01'))

	def parsevideo(self,player):
		playerf = urllib.urlopen(player).read()
		playerf = playerf.split('''<param name="flashvars" value="''')[-1]
		playerf = playerf.split('''"></param>''')[0]
		paramst = playerf.split('&amp;')
		params = {}
		for param in paramst:
			params[param.split('=')[0]]=param.split('=')[1]
		print '*** videoparsing:', paramst
		#if int(params['uid']) <= 0:
		try:
			host = 'cs'+str(int(params['host']))
			if params['is_vk'] == '0':
				host = host + '.vkontakte.ru'
			else:
				host = host + '.vk.com'
		except:
			try:
				host = params['host']
			except:
				pass
		try:
			host = host.split('http://')[-1].split('/')[0]
			uid = '0'*(5-len(params['uid']))+params['uid']
			if int(params['uid']) <= 0:
				return 'http://'+host+"/assets/videos/" + params['vtag']+ params['vkid']+ '.vk.flv'
			else:
				pass
				#~ return 'http://'+host+"/u"+uid+ '/video/' + params['vtag']+ '.flv'
				#this._getHost() + 'u' + this._getUid(vars.uid) + '/video/' + vars.vtag + '.flv';
   
		except:
			print '*** video:playerparser error', paramst



class upload():
	api = None
	def photos(self,aid,filepath,gid=None):
		p=photos()
		p.api=self.api
		UploadServer=p.getUploadServer(aid,gid)
		query = self.api.upload(UploadServer,'file1', filepath)
		print '*** ', query
		if not query['photos_list'] == '':
			query['method']= 'photos.save'
			print '*** photos.save:', query
			save = self.api.gettree(query)
			print '*** ', save
			return 0
		else:
			return -1
			
	def audio(self,filepath,gid=None):
		#pass#getAudioUploadServer
		p=audio()
		p.api=self.api
		UploadServer=p.getUploadServer(gid)
		query = self.api.upload(UploadServer,'file', filepath)
		print '*** ', query
		if not query['audio'] == '':
			p.save(query['server'],query['audio'].replace('%01',chr(1)),query['hash'])
			return 0
		else:
			return -1	

#uid=0
#vid=86549753
#oid=7839870
#host=436.gt3.vkadre.ru
#vtag=ca817dc407c2-
#ltag=30586886
#vkid=77499984
#md_title=%D0%94%D0%B8%D1%82%D0%B0+%D1%84%D0%BE%D0%BD+%D0%A2%D0%B8%D0%B7+%2F+Dita+Von+Teese
#md_author=%D0%A1%D0%B0%D1%88%D1%83%D0%BD%D1%8F+%D0%A5%D1%80%D1%8E%D0%BD%D1%8F
#hd=0
#no_flv=0
#hd_def=0
#dbg_on=0
#t=
#thumb=http://436.gt3.vkadre.ru/assets/thumbnails/3058688677499984.460.vk.jpg
#hash=54568cb4bcd82b15447b8789bd218521
#hash2=63a2798f466c1b23
#is_vk=0
#is_ext=1
#lang_volume_on=%D0%92%D0%BA%D0%BB%D1%8E%D1%87%D0%B8%D1%82%D1%8C+%D0%B7%D0%B2%D1%83%D0%BA
#lang_volume_off=%D0%A3%D0%B1%D1%80%D0%B0%D1%82%D1%8C+%D0%B7%D0%B2%D1%83%D0%BA
#lang_volume=%D0%93%D1%80%D0%BE%D0%BC%D0%BA%D0%BE%D1%81%D1%82%D1%8C
#lang_hdsd=%D0%A1%D0%BC%D0%B5%D0%BD%D0%B8%D1%82%D1%8C+%D0%BA%D0%B0%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%BE
#lang_fullscreen=%D0%9D%D0%B0+%D0%B2%D0%B5%D1%81%D1%8C+%D1%8D%D0%BA%D1%80%D0%B0%D0%BD
#lang_window=%D0%A1%D0%B2%D0%B5%D1%80%D0%BD%D1%83%D1%82%D1%8C
#lang_rotate=%D0%9F%D0%BE%D0%B2%D0%B5%D1%80%D0%BD%D1%83%D1%82%D1%8C
#video_play_hd=%D0%A1%D0%BC%D0%BE%D1%82%D1%80%D0%B5%D1%82%D1%8C+%D0%B2+%D0%B2%D1%8B%D1%81%D0%BE%D0%BA%D0%BE%D0%BC+%D0%BA%D0%B0%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B5
#video_stop_loading=%D0%9E%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D1%8C+%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D1%83
#video_player_version=%D0%92%D0%9A%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%B5+%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE+%D0%BF%D0%BB%D0%B5%D0%B5%D1%80+
#video_player_author=%D0%90%D0%B2%D1%82%D0%BE%D1%80+%D0%BF%D0%BB%D0%B5%D0%B5%D1%80%D0%B0+-+%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9+%D0%A5%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2
#goto_orig_video=%D0%9F%D0%B5%D1%80%D0%B5%D0%B9%D1%82%D0%B8+%D0%BA+%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D0%B8
#video_get_video_code=%D0%9A%D0%BE%D0%BF%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C+%D0%BA%D0%BE%D0%B4+%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE
#video_load_error=%D0%92%D0%B8%D0%B4%D0%B5%D0%BE%D1%84%D0%B0%D0%B9%D0%BB+%D0%B5%D1%89%D0%B5+%D0%BD%D0%B5+%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%B8%D0%BB%D1%81%D1%8F+%D0%B8%D0%BB%D0%B8+%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80+%D1%81+%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE%D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC+%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%BD%D0%BE+%D0%BD%D0%B5%D0%B4%D0%BE%D1%81%D1%82%D1%83%D0%BF%D0%B5%D0%BD
#video_get_current_url=%D0%9A%D0%BE%D0%BF%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C+%D1%81%D1%81%D1%8B%D0%BB%D0%BA%D1%83+%D0%BD%D0%B0+%D0%BA%D0%B0%D0%B4%D1%80
#return this._getHost() + 'u' + this._getUid(vars.uid) + '/video/' + vars.vtag + '.' + size + '.mp4';
#			print param
 		
#~ if __name__ == '__main__':
	#~ v=vk()
	#~ print v.parsevideo('http://vkontakte.ru/video_ext.php?oid=7839870&id=86549753&hash=63a2798f466c1b23')

#junk
		#~ server = server.split('http://')[-1]
		#~ url = '/'+'/'.join(server.split('/')[1:])
		#~ server = server.split('/')[0]
		#~ request = {}
		#~ request[fieldname]=
		#~ 
		#~ conn = httplib.HTTPConnection(server)
		#~ conn.request("POST", "/api.php", request, {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"})
		#~ response = conn.getresponse()
		#~ data = response.read()
		#~ conn.close()

