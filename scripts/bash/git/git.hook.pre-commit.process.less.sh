#!/bin/sh
#Check befor commit if any less files staged
#process it to css and stage both for current commit

LESS=`git diff --name-only --diff-filter=ACMRT --cached | grep \\.less$`

if [ `echo $LESS | wc -l` -ge 1 ]
then
	for f in $LESS
	do
		lessc $f > ${f%%less}css
		git add ${f%%less}css
	done
fi
