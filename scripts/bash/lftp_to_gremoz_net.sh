#!/bin/sh
#Script for uploading data through ftp from commandline to the gremoz.net
#Run: lftp_to_gremoz_net.sh <local_path> <remote_path>

#Configuration BEGIN
local_path=$1
user='u0053093999'
password=`security 2>&1 >/dev/null find-internet-password -gs ftp.hosting-agency.de | sed -e 's/password: "\(.*\)"/\1/g'`
host='ftp.hosting-agency.de'
destination="/public_html/"$2
#Configuration END

commands="open -u $user,$password $host;
mirror -R $local_path $destination;
exit;"
lftp -e "$commands"
