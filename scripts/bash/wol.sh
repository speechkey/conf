#!/bin/bash
# This script sends a WoL packet, checks if remote host is up and displays status notificatioins with growl
# @DEPS http://gsd.di.uminho.pt/jpo/software/wakeonlan/
# @DEPS growlnotify

HOSTNAME="iHome"
IP_ADDRESS=""
MAC_ADDRESS="00:02:44:50:11:94"
PORT=""
REL_PARSER_PATH="../awk"
SCRIPT_PATH=`dirname $0`
GROWL_NOTIFY_PATH="/usr/local/bin"
WOL_PATH="/opt/local/bin"

if [ "$IP_ADDRESS" == "" ];then
	IP_ARGS=""
else
	IP_ARGS=" -i $IP_ADDRESS"
fi

if [ "$PORT" == "" ];then
	PORT_ARGS=""
else
	PORT_ARGS=" -p $PORT"
fi

$WOL_PATH/wakeonlan$IP_ARGS$PORT_ARGS $MAC_ADDRESS > /dev/null
sleep 5
UP_STATUS=`ping -q -c 4 $HOSTNAME | $SCRIPT_PATH/$REL_PARSER_PATH/parse_ping.awk`
$GROWL_NOTIFY_PATH/growlnotify -m "$HOSTNAME is $UP_STATUS."  