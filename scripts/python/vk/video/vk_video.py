#!/usr/bin/env python
# -- coding: utf-8 --
# Import all track from vk acount to the local disk
#

#http://pypi.python.org/pypi/vkontakte
import vkontakte
import re
import argparse
import vk_video_url

#@TODO get credentials from cmd
#@TODO download a few albums
#@TODO get initial token if not already by getting user credentials
#@TODO http://thejosephturner.com/blog/2011/03/19/https-certificate-verification-in-python-with-urllib2/ verify certificate


def main():
    script_args = args()
    #Get user_id and auth token
    #token, user_id = vk_auth.auth(VK_EMAIL,
    #    VK_PASS, VK_APP_ID, VK_SCOPE)
    #create new vk api object

    #Get settings file path

    if not script_args.ACCESS_TOKEN:
        import getpass
        import vk_auth

        password = getpass.getpass()
        try:
            script_args.ACCESS_TOKEN, user_id = vk_auth.auth(script_args.VK_EMAIL, password, script_args.VK_APP_ID, script_args.VK_SCOPE)
        except Exception, message:
            #print "Unable to authorizate a user: " + message.__str__
            print message
            exit()

    if script_args.TOKEN_ONLY:
        print "Access token: " + script_args.ACCESS_TOKEN
        exit()

    vk = vkontakte.API(token=script_args.ACCESS_TOKEN)

    #One can get album track only by album id
    try:
        albumsJson = vk.video.getAlbums(gid=script_args.GROUP_ID, uid=script_args.USER_ID)
    except Exception, data:
        print "Unable to query api: " + data.description
        exit()

    if(script_args.SHOW_ALBUMS):
        print albumsJson
        exit()

    albums = (album for album in albumsJson if isinstance(album, dict))

    if albums:
        if not script_args.VK_PLAYLIST_TITLE:
            print("Which album would you like to syncronze?")
            exit()

        for album in albums:
            if album['title'] == script_args.VK_PLAYLIST_TITLE:
                script_args.ALBUM_ID = album["album_id"]
                moviesJson = vk.video.get(uid=script_args.USER_ID, gid=script_args.GROUP_ID, aid=script_args.ALBUM_ID)
                break
        else:
            print "No audio album found for current user."
            exit()

    for movie in moviesJson:
        if type(movie) == dict:
            if re.search(script_args.MOVIE_TITLE, movie["title"]):
                    print(vk_video_url.getURL(movie["player"]))
            else:
                if script_args.SHOW_MOVIES:
                    print(movie["title"])

#https://ps.userapi.com/c518513/u2048112/videos/f96c0dbc02.360.mp4
# {u'album': u'42994608', u'image_medium': u'http://cs518513.userapi.com/u2048112/video/l_efb69b9e.jpg', u'description': u'<br>[club22771367|TV Shows in English]<br><br>[club22771367|\u0421\u0435\u0440\u0438\u0430\u043b\u044b \u043d\u0430 \u0430\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u043e\u043c \u0432 HD]', u'vid': 164367295, u'title': u'Breaking Bad season 01 episode 01 /  s01e01 / 1  1  [ HD / 720p ] ENG (english)', u'image': u'http://cs518513.userapi.com/u2048112/video/m_7cac4b14.jpg', u'views': 42, u'player': u'http://vkontakte.ru/video_ext.php?oid=-22771367&id=164367295&hash=c140c1a48700783b', u'date': 1352766563, u'link': u'video164367295', u'duration': 2767, u'owner_id': -22771367}]
    #http://cs4990.vkontakte.ru/u13363064/audio/7c1fb39dc000.mp3
    #https://ps.userapi.com/c4990/u13363064/audio/7c1fb39dc000.mp3
    #Start worker processes


def args():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-l', '--log-db-path', dest='LOG_DB_PATH', default='./vk-albums.db', help='sum the integers (default: find the max)')
    parser.add_argument('-t', '--threads', dest='NUMBER_OF_THREADS', default=4, type=int, help='sum the integers (default: find the max)')
    parser.add_argument('-i', '--vk-app-id', dest='VK_APP_ID', default=2951857, type=int, help='sum the integers (default: find the max)')
    parser.add_argument('-s', '--vk-app-secret', dest='VK_APP_SECRET', default='XwIT88vnnYE1oGP0jvXa', help='sum the integers (default: find the max)')
    parser.add_argument('-p', '--vk-app-scope', dest='VK_SCOPE', default='video,audio,offline,wall', help='sum the integers (default: find the max)')
    parser.add_argument('-g', '--gid', dest='GROUP_ID', default=False, help='Group ID of albums owner.')
    parser.add_argument('-u', '--uid', dest='USER_ID', default=False, help='Users ID of albums owner.')
    parser.add_argument('--show-movies', dest='SHOW_MOVIES', default=False, help='Only show movies.')
    parser.add_argument('--aid', dest='ALBUM_ID', default=False, help='Album ID')
    parser.add_argument('-m', '--movie-title', dest='MOVIE_TITLE', default=False, help='Movie title to print url.')
    parser.add_argument('--show-albums', dest='SHOW_ALBUMS', default=False, action='store_true', help='Show all users albums.')
    parser.add_argument('--playlist', dest='VK_PLAYLIST_TITLE', default=False, help='Playlist title to fetch.')
    parser.add_argument('--url-only', dest='URL_ONLY', default=False, action='store_true', help='Print only track urls: Artist: <artist> | Title: <title> | Url: <url>')
    parser.add_argument('-e', '--vk-email', dest='VK_EMAIL', help='Vk users email.')
    parser.add_argument('--token-only', dest='TOKEN_ONLY', default=False, action='store_true', help='Display only token after successful authorization.')
    parser.add_argument('-a', '--access-token', dest='ACCESS_TOKEN', default=False, help='VK App access token.')

    args = parser.parse_args()
    return args


main()




#if __name__ == '__main__':
#    main()
