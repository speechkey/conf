#!/usr/bin/env bash
##################################################################################
#
# Checks if commands available in current shell environment and tries
# to install them with brew. Prints execution log to stdout. Exits with 0 if all
# dependecies passed in argument are available or with 1 if not.
#
# TODO:
#       # Check OS before using brew
#
# Usage:
#       check-cmd-exists.sh checkstyle jq
#
# Author: Artem Grebenkin <speechkey@gmail.com>
# Date: 09.10.2014
#
##################################################################################

# For using scripts in dev-utils
PATH=$PATH:$SK_BIN/bash/dev-utils

for dependency in "$@"; do
    # Check dependencies and try to install if not exists
    if [[ -z $(command -v $dependency) ]]; then
        echo "Installing \`$dependency\` with brew..." | colorize-shell.sh yellow ".*"
        #try to install dependency
        brew update 2>/dev/null 1>&2 && brew install $dependency 2>/dev/null 1>&2
        # Check if has fail to install
        if [[ $? -eq 1 ]]; then
            echo "Error: Unable to install \`$dependency\`." | colorize-shell.sh red ".*"
            exit 1
        else
            echo "\`$dependency\` was successfully installed."  | colorize-shell.sh green ".*"
        fi
    fi
done
# Exit ok if all dependencies are present
exit 0
