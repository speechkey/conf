#!/usr/bin/env bash
##################################################################################
#
# Checks java files on pmd issues with given ruleset. Is able to print
# execution log to stdout and define priority level to fail. Exits with 1 if
# pmd issues with priority over level was found or with 1 if not.
#
# Usage:
#       inspect-pmd.sh --silent --level 3 --ruleset <path_to_the_ruleset> ~/TestClass.java ~/TestClass2.java
#
#            --ruleset: Path to the pmd ruleset
#            --silent: Optional. Don't log pmd output
#            --level: Optional. Pmd notification level to fail, default `3`
#
# Author: Artem Grebenkin <speechkey@gmail.com>
# Date: 09.10.2014
#
##################################################################################

# For using scripts in dev-utils
PATH=$PATH:$SK_BIN/bash/dev-utils
# Check dependencies
#check-cmd-exists.sh findbugs || exit 1
_expand(){
	# This is part of the rules of ~-expansion. It is clearly stated in the Bash manual that this expansion is not performed when the ~ is quoted.
	# http://stackoverflow.com/questions/16703624/bash-interpret-string-variable-as-file-name-path
	T=$(echo $1 | sed 's/^~/$HOME/')
	cd "$T";pwd
	perl -MCwd -e 'print Cwd::realpath($ARGV[0])' "$T"
	echo $TEST
}

_setArgs(){
	# Print script usage
	printUsage(){
		# Define usage message
		read -d '' USAGE <<EOF
Usage: inspect-findbugs.sh [ --silent | -s ] [ --level=<level> ] [ --ruleset=<path_to_the_ruleset> ] [ paths to files to inspect ]

	-s, --silent:     	  Optional. Don't log pmd output
	--ruleset=<path>:     Path to the pmd ruleset
	--level=<level>:      Optional. Pmd notification level to fail, default medium
	--y-ext-root=<path>:  Path to the hybris extensions root
	--auxclasspath=<path>:Path to the temporary aux class path file
EOF
		# Print usage message and colorize them
		echo "$USAGE" \
			| colorize-shell.sh yellow '-.*:' `# colorize options` \
			| colorize-shell.sh yellow 'Usage:' >&2 `# colorize Usage:`
	}
	# Print option not found error message
	printUnknownOption(){
		echo "Error: Unknown option --$1" | colorize-shell.sh red 'Error' >&2
	}

	optspec=":sh-:"
	while getopts "$optspec" optchar; do
	    case "${optchar}" in
	        -)
	            case "${OPTARG}" in
	                silent)
	                    ARG_SILENT=1
	                    ;;
	                ruleset=*)
	                    ARG_RULESET=${OPTARG#*=}
	                    ;;
	                y-ext-root=*)
	                    ARG_Y_EXT_ROOT=${OPTARG#*=}
	                    ;;
	                auxclasspath=*)
	                    ARG_AUXCLASSPATH=${OPTARG#*=}
	                    ;;
	                level=*)
	                    ARG_LEVEL=${OPTARG#*=}
	                    ;;
	                help)
	                    printUsage >&2
	                    exit 2
	                    ;;
	                *)
					# Unknown option, so print error, usage and exit with error code
						printUnknownOption ${OPTARG} >&2
	                    printUsage >&2
	                    ;;
	            esac;;
	        h)
				printUsage >&2
	            exit 2
	            ;;
	        s)
	            ARG_SILENT=1
	            ;;
	        *)
				# Unknown option, so print error, usage and exit with error code
				printUnknownOption ${OPTARG} >&2
				printUsage >&2
				exit 1
	            ;;
	    esac
	done

	shift $(($OPTIND - 1))
	ARG_INSPECT_FILES=$@
}

# parse arguments
_setArgs "$@"
# Check for non-optional arguments
if [[ -z $ARG_INSPECT_FILES ]]; then
	echo "Error: No classes to analyse." | colorize-shell.sh red 'Error:'
	exit 1
fi

if [[ -z $ARG_Y_EXT_ROOT ]]; then
	echo "Error: No hybris extensions path given. Unable to generate auxclasspath." | colorize-shell.sh red 'Error:'
	exit 1
fi

if [[ -z $ARG_Y_EXT_ROOT ]]; then
	echo "Error: No hybris extensions path given. Unable to generate auxclasspath." | colorize-shell.sh red 'Error:'
	exit 1
fi

# prepare argument for findbugs util
[[ ! -z $ARG_RULESET ]] && ARG_RULESET="-exclude $ARG_RULESET" || ARG_RULESET=""
[[ ! -z $ARG_LEVEL ]] && ARG_LEVEL="-$ARG_LEVEL" || ARG_RULESET=""
[[ -z $ARG_AUXCLASSPATH ]] && ARG_AUXCLASSPATH="./.auxclasspath"

CLASS_DIRS=""
QUALIFIED_NAMES=""
for JAVA_FILE_PATH in $ARG_INSPECT_FILES; do
	# expand paths
	JAVA_FILE_PATH=$(greadlink -f $JAVA_FILE_PATH)
	# -onlyAnalyze takes full qualified name of the java class
	# extract class package name
	PACKAGE_NAME=$(cat $JAVA_FILE_PATH | grep 'package' | sed 's/package \(.*\);/\1/')
	# exctract class name and append them to the package name to get full qualified class name
	QUALIFIED_NAMES=$QUALIFIED_NAMES"-onlyAnalyze \""$PACKAGE_NAME"."$(basename $JAVA_FILE_PATH | sed 's/\(.*\)\.java/\1/')"\" "
	# Add a new line character to be able to remove duplicates by `sort -u`
	CLASS_DIRS=$CLASS_DIRS$(dirname $JAVA_FILE_PATH | sed 's/\/src\//\/classes\//')"\n"
done
# remove duplicate paths
CLASS_DIRS=$(echo -e "$CLASS_DIRS" | sort -u | sed ':a;N;$!ba;s/\n/ /g')

ARG_Y_EXT_ROOT=$(echo $ARG_Y_EXT_ROOT | sed "s/^~/"$HOME"/")
echo $ARG_Y_EXT_ROOT
mdfind 'kMDItemFSName == "*.jar" || kMDItemFSName == "classes"' -onlyin $ARG_Y_EXT_ROOT | sed '/apache-ant/d' | sed '/\/ant\//d' | sed '/source/d' | sed '/javadoc/d' > $ARG_AUXCLASSPATH

echo "findbugs -textui $ARG_LEVEL $ARG_RULESET $QUALIFIED_NAMES -auxclasspathFromFile $ARG_AUXCLASSPATH" $CLASS_DIRS

