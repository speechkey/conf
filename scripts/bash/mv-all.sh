# Move all elements from source to the same directory in destination folder, an create links in source folder on elements
# in the destination folder
# @depend: realpath
# mv-all.sh (file|dir) <source folder path> <destination folder path> 

IFS=$'\n'

#Convert paths to absolute
dest=`realpath $3`
source=`realpath $2`
base_source=`basename $source`
#Check for what kind of elements to create links 
if [ "$1" == "files" ];
then
        type='f'
else
        type='d'
fi
#Check if source directory exists in target folder 
if [ ! -d $dest/$base_source ];
then
	mkdir $dest/$base_source
fi

for file in `find $source -type $type -depth 1`;
do
	base_file=`basename $file`
	mv -v $file $dest/$base_source
	if [ $? -eq 0 ];
	then
		ln -s $dest/$base_source/$base_file $file
	fi
	echo -e "\033[31m$base_source/$base_file\033[0m -> \033[32m$dest/$base_source/$base_file\033[0m"
done

unset $IFS
