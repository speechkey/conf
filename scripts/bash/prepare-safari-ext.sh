#!/bin/sh
# Prepare development environment for a new extension
# Run prepare-safari-ext <ext_key> <ext_name> <ext_path>

#Extension key
EXT_KEY=$1
#Quoting extension name for passing to the bash script
EXT_NAME=$(echo $2 | sed 's/ /\\ /g')
#Extension path
EXT_PATH=$3
#Path to the deploy file creator
DEPLOY_PATH=~/Dev/scripts/bash/deploysafext.sh

#Create dev folder
mkdir -p $EXT_PATH/dev/js

#Add js files
#Add new extension js file
touch $EXT_PATH/dev/js/safari-ext.$EXT_KEY.js

#Download current version of jQuery
wget -O $EXT_PATH/dev/js/jquery.min.js http://code.jquery.com/jquery.min.js

#Create deploy script
echo "$DEPLOY_PATH $EXT_KEY $EXT_NAME" > $EXT_PATH/deploy.sh
chmod u+x $EXT_PATH/deploy.sh
