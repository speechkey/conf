#!/bin/bash
#
#
#ffmpeg -y -i $1 -ar 16000 test.flac
#
#This runs in background, and prints pid
#rec -q -r 16000 -b 16 -c 1 test.wav 1>/dev/null 2>/dev/null &
#Arter recording kill by pid
#kill -9 <pid>
# this is possible with rec stop by silence of 2 sec for example, check out
# Nice shot to use it with wolframalpha http://cranklin.wordpress.com/2012/01/13/building-my-own-siri-jarvis/

## Implementation details
#  @dependecy JSON.sh to parse json https://github.com/dominictarr/JSON.sh
#  First script invocation records voice
#  Second script invocation saves recording converts it, sends to google and parse output
PATH="/usr/bin:/bin:/usr/sbin:/sbin"
REC_PID=`ps aux | awk '/\/tmp\/voice-recognition.wav/ { print $2}'`
if  [ -z "$REC_PID" ]
then
	/opt/local/bin/rec -r 16000 -b 16 -c 1 /tmp/voice-recognition.wav
else
	kill -9 $REC_PID
 	/opt/local/bin/sox /tmp/voice-recognition.wav /tmp/voice-recognition.flac gain -n -5 silence 1 5 2%
 	curl -s -X POST --data-binary @/tmp/voice-recognition.flac --user-agent 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7' --header 'Content-Type: audio/x-flac; rate=16000;' 'https://www.google.com/speech-api/v1/recognize?client=chromium&lang=ru-RU&maxresults=10' | /Users/speechkey/Dev/scripts/bash/JSON.sh/JSON.sh -b -p | grep "\[\"hypotheses\",0,\"utterance\"\]" | sed -e 's/\["hypotheses",0,"utterance"\][[:space:]]*"\(.*\)"/\1/'
 	rm -rf /tmp/voice-recognition.wav
fi
