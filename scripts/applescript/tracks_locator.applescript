tell application "iTunes"
	set allowed_comments to {"Remote on Audio", "Remote on NAS", "Remote on 160", "Not Found!"}
	
	set selectedTracks to selection
	if selectedTracks is {} then return
	
	if (get class of (item 1 of selectedTracks)) is not file track then return
	
	repeat with trk in selectedTracks
		try
			set track_location to (location of trk as string)
			
			set chk to do shell script "if [[ \"" & my quofo(track_location) & "\" == *Volumes/Media* ]];then echo \"Remote on Audio\"; elif [[ \"" & my quofo(track_location) & "\" == *Volumes/NAS* ]];then echo \"Remote on NAS\";elif [[ \"" & my quofo(track_location) & "\" == *Volumes/160* ]];then echo \"Remote on 160\";elif [[ ! -f " & my quofo(track_location) & " ]];then echo \"Not Found!\";fi"
			
			if allowed_comments contains chk then
				my mark(trk, chk)
			else
				my mark(trk, "Local")
			end if
			
		on error m
			my logger(m & (artist of trk as string) & " - " & (name of trk as string) & " : " & (location of trk as string))
		end try
	end repeat
end tell

on quofo(t)
	return (quoted form of POSIX path of (t as text))
end quofo

on mark(t, nf)
	tell application "iTunes" to set comment of t to nf
end mark

to logger(t)
	log t
	try
		do shell script "logger -t Relocator " & quoted form of (t as text)
	end try
end logger