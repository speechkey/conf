
# Stop local MySQL if running
if pgrep mysql ; then
	$SK_BIN/bash/service-manager.sh mysql stop
fi
# Kill all MySQL tunnels
kill `ps aux | grep ssh | grep 3306 | grep -v grep | tr -s " " | cut -d " " -f 2`
# Forward local MySQL port to test2
autossh -M 0 -q -f -N -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3" -L 3306:127.0.0.1:3306 agrebenkin@test2.simply-local.de
# Stop local ES if running
if pgrep -f elasticsearch ; then
	$SK_BIN/bash/service-manager.sh elasticsearch stop
fi
# Kill all ES tunnels
kill `ps aux | grep ssh | grep 9200 | grep -v grep | tr -s " " | cut -d " " -f 2`
kill `ps aux | grep ssh | grep 9300 | grep -v grep | tr -s " " | cut -d " " -f 2`
# Forward local ES port to ES cluster
autossh -M 0 -q -f -N -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3" -L 9200:54.216.236.51:9200 aws@repo.my-xplace.de
autossh -M 0 -q -f -N -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3" -L 9300:54.216.236.51:9300 aws@repo.my-xplace.de
