#!/bin/bash
# Force to use bash
if [ "$(ps -p "$$" -o comm=)" != "bash" ]; then
    bash "$0" "$@"
    exit "$?"
fi

# Task configuration
HAC_USERNAME="admin"
HAC_PASSWORD="nimda"
HAC_DOMAIN="hldlx04.berner.lan"
HAC_PORT="9001"
HAC_CONTEXT_PATH=""
# Task configuration end

HAC_URL=$HAC_DOMAIN:$HAC_PORT$HAC_CONTEXT_PATH

yauth () {
	# Authorize client
	PARAMS=$(curl --silent http://$HAC_URL/login.jsp | sed -n -e '/meta.*_csrf/{p;n;}' -e '/<form/{p;n;}' | sed 's/.*name=\"_csrf\" content=\"\(.*\)\".*/\1/' | sed 's/.*action=\"\([^"]*\)\".*/\1/' | tr '\n' ' ')
	AUTH_CSRF=$(echo $PARAMS | cut -d ' ' -f 1)
	ACTION_URL=$(echo $PARAMS | cut -d ' ' -f 2)
	COOKIE=$(echo $ACTION_URL | cut -d ';' -f 2)

	AUTH_COOKIE=$(curl -s -i --data "_csrf=$AUTH_CSRF&j_username=$HAC_USERNAME&j_password=$HAC_PASSWORD" --cookie "$COOKIE" http://$HAC_URL$ACTION_URL | sed '/Set-Cookie: JSESSIONID.*/!d' | sed 's/Set-Cookie: \(JSESSIONID=[^ ]*\);.*/\1/')

	CSRF=$(curl -i -s --cookie "$AUTH_COOKIE" http://$HAC_URL/platform/update | sed -n -e '/meta.*_csrf/{p;n;}' | sed 's/.*name=\"_csrf\" content=\"\(.*\)\".*/\1/')
}

echo "Waiting for HAC is up..."
sleep 5
curl -i -s http://$HAC_URL/login.jsp >/dev/null 2>&1

# Authenticate at HAC
yauth

# Check authenticateion status
[[ ${#AUTH_COOKIE} > 15 && ${#CSRF} > 15 ]] && echo "DEBUG: Successfully authenticated on HAC with cookie: $AUTH_COOKIE and csrf: $CSRF." || ( echo "ERROR: Unable to authenticate on HAC." && exit 1)

# Configure URS over JSON sent to the server:
# - To disable update running system set initMethod to null:
#   "initMethod":null
# - To enable/disable one of the main URS options like
#   'Clear the hMC configuration from the database' set
#   correspondend property e.g. clearHMC to true/false. To
#   figure out which option has whiich property key inspect
#   property checkbox id in HAC ${HAC_URL}/platform/update.
# - To enable import extensions sample data impexes
#   during URS set extension key to true in "addParameters"
#   object sent with request e.g.
#   "allParameters": {"core_sample": "true"} will import all
#   core extensions sample data impexes.
# - To enable an option in extension sample data import enable
#   the extension sample data impexes import, inspect desired
#   option html node id in HAC ${HAC_URL}/platform/update
#   which will be the key and correspondent value and add them
#   also to the  "addParameters" object sent with request e.g.
#   "allParameters": {
#       "factfindersearch_sample":"true",
#		"factfindersearch_importFactfinderConfig":["yes"]
#	}
#
read -d '' JSON << EOF
{
	"initMethod":"UPDATE",
	"dropTables":false,
	"clearHMC":false,
	"createEssentialData":false,
	"localizeTypes":false,
	"allParameters":{
		"factfindersearch_sample":"true",
		"factfindersearch_importFactfinderConfig":["yes"]
	}
}
EOF

echo "DEBUG: Executing update running system..."
URS_LOG=$(curl -s -i http://$HAC_URL/platform/init/execute \
	--compressed \
	-H "Cookie: $AUTH_COOKIE" \
	-H "Accept: application/json" \
	-H "X-CSRF-TOKEN: $CSRF" \
	-H "X-Requested-With: XMLHttpRequest" \
	-H "Content-Type: application/json; charset=UTF-8" \
	--data-binary "$JSON")

URS_STATUS=$(echo $URS_LOG | grep '"success":true' | wc -l)
[[ $URS_STATUS -eq 1 ]] && echo "DEBUG: Update running system was successfull." || ( echo "ERROR: Update running system failed:" $URS_LOG && exit 1 )
